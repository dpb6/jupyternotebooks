# Programming for Financial Technology
The guide provides much of the programming materials used in Duke University’s [Masters of Engineering In Financial Technology](https://fintech.meng.duke.edu/) program.

You can access a static version of the guide at https://fintechpython.pages.oit.duke.edu/jupyternotebooks/intro.html

To install the guide locally (allows for interactive use), follow the instructions in 
[Establishing Your Environment](https://fintechpython.pages.oit.duke.edu/jupyternotebooks/0-Preliminaries/01-Environment.html).

