{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "87ddd37c-6aee-446e-8de1-c44b33bf7bb3",
   "metadata": {},
   "source": [
    "# Decorators\n",
    "\n",
    "Function decorators in Python are a powerful and expressive feature for modifying the behavior of functions or methods. They allow you to \"decorate\" a function without changing its structure, enhancing its functionality or altering its behavior.\n",
    "\n",
    "At the core, a decorator is a function that takes another function as an argument and extends its behavior without explicitly modifying it. This is achieved by wrapping the original function inside a new function.\n",
    "\n",
    "## A Simple Example\n",
    "In the following example, we have a simple decorator that prints a message before and after the \"decorated\" function is called.\n",
    "\n",
    "First, we show explicitly wrapping a function by passing a function to the decorator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad7a7e44-94de-4133-9207-372038b6a198",
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_decorator(func):\n",
    "    def wrapper():\n",
    "        print(\"Before the function is called.\")\n",
    "        func()\n",
    "        print(\"After the function is called.\")\n",
    "    return wrapper\n",
    "\n",
    "def say_hello():\n",
    "    print(\"Hello!\")\n",
    "\n",
    "say_hello = my_decorator(say_hello)\n",
    "say_hello()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88b1b304-a9be-4715-846a-e7c4ca061c67",
   "metadata": {},
   "source": [
    "Note: The names used for the `func` parameter and the inner function (e.g., `wrapper`) are not fixed - you can choose different names, you just need to follow the pattern.\n",
    "\n",
    "More typically, we will use the `@` symbol to decorate a function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d6920762-43f1-4a0f-ab25-adfcd8e3d031",
   "metadata": {},
   "outputs": [],
   "source": [
    "@my_decorator\n",
    "def hello_world():\n",
    "    print(\"Hello World!\")\n",
    "\n",
    "hello_world()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07106f43-fe18-4fdf-aa38-276d0ad41697",
   "metadata": {},
   "source": [
    "In this case, the functionality of `my_decorator` will always be performed whenever the function `hello_world()` is called"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0fe3510b-a3fd-4641-b23a-35375c39b49a",
   "metadata": {},
   "source": [
    "## Decorating Functions With Arguments\n",
    "To decorate functions that accept arguments, modify the wrapper function to accept arguments.  As we do not necessarily know the number of arguments, we'll use `*args` and `**kwargs` to pack/unpack  *positional* and *named* parameters.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "530440e1-8f7f-407d-93f6-adf49ba88039",
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_decorator(func):\n",
    "    def wrapper(*args, **kwargs):\n",
    "        print(\"Before the function is called.\")\n",
    "        result = func(*args, **kwargs)\n",
    "        print(\"After the function is called.\")\n",
    "        return result\n",
    "    return wrapper\n",
    "\n",
    "@my_decorator\n",
    "def greet(name):\n",
    "    \"\"\"Prints hello to someone\"\"\"\n",
    "    print(\"Hello {}!\".format(name))\n",
    "\n",
    "greet(\"Alice\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01f222f1",
   "metadata": {},
   "source": [
    "## Introspection on Decorated Functions\n",
    "As everything (including functions) are objects in Python, we can use instrospection to examine the properties of objecs at runtime.  So we can use the \"dunder\" properties to get a function's name and documentation string(docstring).\n",
    "\n",
    "However, when we wrap/decorate a function, by default, we return back the wrapping function's name and docstring:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b25ffc81",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Name:\", greet.__name__, \"\\nDoc String:\",greet.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb85a1f2",
   "metadata": {},
   "source": [
    "To fix this issue, we need to use the @functools.wraps decorator to preserve the orginal function's attributes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5f54368e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import functools\n",
    "\n",
    "def my_decorator(func):\n",
    "    @functools.wraps(func)\n",
    "    def wrapper(*args, **kwargs):\n",
    "        print(\"Before the function is called.\")\n",
    "        result = func(*args, **kwargs)\n",
    "        print(\"After the function is called.\")\n",
    "        return result\n",
    "    return wrapper\n",
    "\n",
    "@my_decorator\n",
    "def greet(name):\n",
    "    \"\"\"Prints hello to someone\"\"\"\n",
    "    print(\"Hello {}!\".format(name))\n",
    "\n",
    "greet(\"Bob\")\n",
    "print(\"\\nName:\", greet.__name__, \"\\nDoc String:\",greet.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "071b2d2e-4a21-4acf-9500-df2817181919",
   "metadata": {},
   "source": [
    "## Example Usages\n",
    "\n",
    "### Logging\n",
    "We can use decorators to log function calls:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40f32b72-ea00-4125-9cbb-8a755b66cc93",
   "metadata": {},
   "outputs": [],
   "source": [
    "import functools\n",
    "\n",
    "def log_function_call(func):\n",
    "    @functools.wraps(func)\n",
    "    def wrapper(*args, **kwargs):\n",
    "        print(\"Function:\", func.__name__)    # remember, functions are objects\n",
    "        print(\"Positional arguments:\", args)\n",
    "        print(\"Keyword arguments:\", kwargs)\n",
    "        result = func(*args, **kwargs)\n",
    "        print(\"Result:\",result);\n",
    "        return result\n",
    "    return wrapper\n",
    "\n",
    "@log_function_call\n",
    "def add(x, y):\n",
    "    \"\"\"Adds two numbers\"\"\"\n",
    "    return x + y\n",
    "\n",
    "print(add(1800, 42))\n",
    "print(add.__doc__)    # Another demostration of using @functools.wrap"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d05c1ba-9b3d-449f-9e40-d3b583588fe1",
   "metadata": {},
   "source": [
    "## Performance Testing \n",
    "\n",
    "Decorators can also be used to measure the execution time of a function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d0406ba-ba1c-489c-8d4a-2207bcf16726",
   "metadata": {},
   "outputs": [],
   "source": [
    "import functools\n",
    "import time\n",
    "\n",
    "def timing_decorator(func):\n",
    "    @functools.wraps(func)\n",
    "    def wrapper(*args, **kwargs):\n",
    "        start_time = time.time()\n",
    "        result = func(*args, **kwargs)\n",
    "        end_time = time.time()\n",
    "        print(\"Executing {} took {} seconds\".format(func.__name__, end_time - start_time))\n",
    "        return result\n",
    "    return wrapper\n",
    "\n",
    "@timing_decorator\n",
    "def slow_function():\n",
    "    time.sleep(2)\n",
    "\n",
    "slow_function()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb9a44b8",
   "metadata": {},
   "source": [
    "## Decorators with Arguments\n",
    "When you want a decorator to accept arguments, you need to add another function layer:\n",
    "- **Outer function**: This function takes the decorator arguments.\n",
    "- **Middle function**: This function takes the function to be decorated.\n",
    "- **Inner function (wrapper)**: This function wraps original function that adds the extra functionality.\n",
    "\n",
    "So the general structure appears as -\n",
    "```python\n",
    "def decorator_with_arguments(arg1, arg2, ...):\n",
    "    def middle_function(func):\n",
    "        @functools.wraps(func)            # Used to maintain decoratored function's \"meta\" information (e.g., docstring)\n",
    "        def wrapper(*args, **kwargs):\n",
    "            # Code to execute before calling the original function\n",
    "            print(\"Decorator arguments: {}, {}, ...\".format(arg1, arg2, ...))\n",
    "            result = func(*args, **kwargs)\n",
    "            # Code to execute after calling the original function\n",
    "            return result\n",
    "        return wrapper\n",
    "    return middle_function\n",
    "```\n",
    "\n",
    "**Building upon the performance testing use case:**\n",
    "\n",
    "One of the difficulties with measuring code performance is that a function executes too fast to measure a function's execution time. To work around\n",
    "this limitation, execute the function many times to gain an accurate sample. In our exaample, we can modify the timing decorator to take an \n",
    "optional parameter for the number of executions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "2f45da58",
   "metadata": {},
   "outputs": [],
   "source": [
    "import functools,  time\n",
    "\n",
    "def timing_decorator(execution_count = 1):\n",
    "    def middle_function(func):\n",
    "        @functools.wraps(func)\n",
    "        def wrapper(*args, **kwargs):\n",
    "            start_time = time.time()\n",
    "            for _ in range(execution_count):\n",
    "                result = func(*args, **kwargs)\n",
    "            end_time = time.time()\n",
    "            print(\"Executing {} {} times() took {} seconds\".format(func.__name__, execution_count, end_time - start_time))\n",
    "            return result\n",
    "        return wrapper\n",
    "    return middle_function\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "44da3fe4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Executing slow_function 3 times() took 3.008880853652954 seconds\n"
     ]
    }
   ],
   "source": [
    "@timing_decorator(3)\n",
    "def slow_function(seconds):\n",
    "    \"\"\" function sleeps for a certain number of seconds\"\"\"\n",
    "    time.sleep(seconds)\n",
    "\n",
    "slow_function(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a356f3f0",
   "metadata": {},
   "source": [
    "## Closures and Decorators\n",
    "A [closure](https://en.wikipedia.org/wiki/Closure_(computer_programming)) is a function object that\n",
    "retains bindings to the variables that were in its lexical scope \n",
    "when the function was created, even after those variables would normally go out of scope. In other words, \n",
    "a closure allows a function to access variables from an enclosing scope, even after that scope has finished executing.\n",
    "\n",
    "Closures are created by defining a function inside another function and then returning the inner function. \n",
    "The inner function has access to the variables of the outer function.\n",
    "\n",
    "Here's a simple example to illustrate closures:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "438d005a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello, World!\n"
     ]
    }
   ],
   "source": [
    "def outer_function(message):\n",
    "    def inner_function():\n",
    "        print(message)\n",
    "    return inner_function\n",
    "\n",
    "closure = outer_function(\"Hello, World!\")\n",
    "closure()  # This will print \"Hello, World!\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c4cb396",
   "metadata": {},
   "source": [
    "- `outer_function` takes a parameter `message` and defines an inner function `inner_function` that prints `message`.\n",
    "- `outer_function` returns `inner_function`, which is a closure because it \"remembers\" the value of `message` even after `outer_function` has finished executing."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b37a8d30",
   "metadata": {},
   "source": [
    "### How Closures Are Used to Implement Decorators\n",
    "\n",
    "Decorators rely on closures to modify the behavior of functions or methods. Here’s a step-by-step explanation:\n",
    "\n",
    "1. **Outer Function**: The outer function is called when the decorator is applied. It can take arguments if needed.\n",
    "2. **Inner Function (Wrapper)**: The inner function, or wrapper, is defined inside the outer function and has access to the outer function’s variables.\n",
    "3. **Returning the Wrapper**: The outer function returns the inner function, which becomes the decorated function.\n",
    "\n",
    "### Example of a Basic Decorator Using Closures\n",
    "\n",
    "Let’s create a simple decorator that adds logging around a function call:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "b38b6b13",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Calling function add with arguments (2, 3) and keyword arguments {}\n",
      "Function add returned 5\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "5"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def simple_logger(func):\n",
    "    def wrapper(*args, **kwargs):\n",
    "        print(\"Calling function {} with arguments {} and keyword arguments {}\".format(func.__name__,args,kwargs))\n",
    "        result = func(*args, **kwargs)\n",
    "        print(\"Function {} returned {}\".format(func.__name__,result))\n",
    "        return result\n",
    "    return wrapper\n",
    "\n",
    "@simple_logger\n",
    "def add(a, b):\n",
    "    return a + b\n",
    "\n",
    "add(2, 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "faed20e4",
   "metadata": {},
   "source": [
    "In this example:\n",
    "- `simple_logger` is the outer function that takes the original function `func` as an argument.\n",
    "- `wrapper` is the inner function (closure) that logs the function call and its result.\n",
    "- `simple_logger` returns `wrapper`, so when `add` is called, the `wrapper` function is actually executed.\n",
    "\n",
    "### Example of a Decorator with Arguments Using Closures\n",
    "\n",
    "Let’s create a decorator that logs messages with a custom prefix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "102402b5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "DEBUG: Calling function multiply\n",
      "DEBUG: Function multiply returned 12\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "12"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def logger(prefix):\n",
    "    def decorator(func):\n",
    "        def wrapper(*args, **kwargs):\n",
    "            print(\"{} Calling function {}\".format(prefix,func.__name__))\n",
    "            result = func(*args, **kwargs)\n",
    "            print(\"{} Function {} returned {}\".format(prefix,func.__name__,result))\n",
    "            return result\n",
    "        return wrapper\n",
    "    return decorator\n",
    "\n",
    "@logger(\"DEBUG:\")\n",
    "def multiply(a, b):\n",
    "    return a * b\n",
    "\n",
    "multiply(3, 4)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3c6d68b9",
   "metadata": {},
   "source": [
    "- `logger` is the outer function that takes a prefix string as an argument and returns the actual decorator `decorator`.\n",
    "- `decorator` is a function that takes the original function `func` and returns the `wrapper` function.\n",
    "- `wrapper` is the closure that has access to both the original function `func` and the `prefix` from `logger`.\n",
    "- `logger(\"DEBUG:\")` returns `decorator`, which is then applied to `multiply`.\n",
    "\n",
    "### Why Closures Are Useful for Decorators\n",
    "Closures allow the decorator to retain state (such as arguments) and modify the behavior of functions or methods in a flexible way. \n",
    "By leveraging closures, decorators can:\n",
    "- Access and manipulate variables from the enclosing scope.\n",
    "- Maintain state across multiple calls.\n",
    "- Provide a clean and readable way to extend or alter the behavior of functions and methods."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "734ba972",
   "metadata": {},
   "source": [
    "## Decorators and Flask\n",
    "While we'll cover Flask in more detail in Section 5, we do want to highlight a couple of uses of decorators within Flask and web-development in general.\n",
    "\n",
    "### Register Route Handlers\n",
    "One of the tasks that needs to occur within web applications is to map URLs to the functions that process a specific function.  To do that, we use a the \"@route\" decorator that takes a URL pattern as an argument.  The HTTP Method may also be specified in the route as well.\n",
    "\n",
    "```python\n",
    "from flask import render_template\n",
    "\n",
    "@app.route('/hello/<name>')\n",
    "def hello(name=None):\n",
    "    return render_template('hello.html', name=name)\n",
    "```\n",
    "\n",
    "### Require User Authentication\n",
    "\n",
    "```python\n",
    "def login_required(view):\n",
    "    @functools.wraps(view)\n",
    "    def wrapped_view(**kwargs):\n",
    "        if g.user is None:\n",
    "            return redirect(url_for('auth.login'))\n",
    "\n",
    "        return view(**kwargs)\n",
    "\n",
    "    return wrapped_view\n",
    "\n",
    "# source: https://flask.palletsprojects.com/en/3.0.x/tutorial/views/\n",
    "```\n",
    "\n",
    "By applying the `@login_required` decorator, we check if a user ID has been established in the current request (another function performs this if the user ID is in the current session).  If the user object is not set, the application redirects the user to the login page. Otherwise, the normal route function(view) is called and returned.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4fc42114-137c-4135-b3ef-8eed486d8fdc",
   "metadata": {},
   "source": [
    "## Aspect-Oriented Programming and Decorators\n",
    "\n",
    "[Aspect-Oriented Programming (AOP)](https://en.wikipedia.org/wiki/Aspect-oriented_programming) and Python decorators share a common principle: both techniques aim to separate cross-cutting concerns from the main business logic of a program. While AOP is more commonly associated with languages like Java, Python decorators offer a way to implement aspects of AOP in Python. \n",
    "\n",
    "AOP aims to increase modularity by allowing the separation of cross-cutting concerns (like logging, security, or error handling) by adding additional behavior to existing code (\"an advice\") without modifying the code itself.\n",
    "\n",
    "A decorator is a syntactic feature that adds functionality to an existing function or method (known as advice in AOP terminology). They can be used to implement AOP-like features in Python, handling cross-cutting concerns by \"decorating\" functions or methods.\n",
    "\n",
    "### How They Work Together\n",
    "*Implementing Advice*: In AOP, advice is additional code you want to run at certain points in your program. In Python, decorators can wrap a function or method to execute code before or after the wrapped function, similar to \"before\" and \"after\" advice in AOP.\n",
    "\n",
    "*Pointcut and Join Points*: In AOP, a pointcut defines where an advice should be applied, and a join point is a specific point, like method execution. While Python doesn't have a native concept of pointcuts, decorators can be selectively applied to functions or methods, acting like a manual pointcut.\n",
    "\n",
    "*Separation of Concerns*: Both AOP and decorators help to separate concerns, such as logging, error handling, or performance measurement from the main business logic. This makes the code cleaner and more maintainable.\n",
    "\n",
    "*Dynamic Behavior*: Just like AOP, decorators can dynamically add behavior to functions or methods without altering their actual code, which is especially useful in scenarios like authorization, where certain actions need to be performed based on dynamic context.\n",
    "\n",
    "### Example: click\n",
    "While not strictly an AOP framework, the [click](https://click.palletsprojects.com/) library, which is used to create command line interfaces,\n",
    "demonstrates these principles.  Primarily,  `click` helps *separate the concern* of command-line interface handling from the business logic \n",
    "of the application. This separation allows you to focus on the core functionality of your commands without worrying about \n",
    "how to parse arguments or handle user inputs. This additional behavior is *dynamic* in that the original function does not need to be modified.\n",
    "The *advice* in this situation is the handling of command-line arguments in a reusable library.\n",
    "\n",
    "## Summary\n",
    "Decorators are a very powerful and useful tool in Python. They allow for cleaner and more readable code, especially when adding common functionality to multiple functions or methods. Understanding and using decorators can greatly enhance your Python programming skills.\n",
    "\n",
    "Remember, while decorators are powerful, they should be used judiciously, as they can sometimes make your code more complex and harder to understand."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d604924-40d2-4199-93e1-94b2f1cf1bed",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
