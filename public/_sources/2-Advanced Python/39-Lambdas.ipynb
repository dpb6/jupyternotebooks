{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "85f08f11-e5a4-4e91-9415-3d95e36dd371",
   "metadata": {},
   "source": [
    "# Lambdas\n",
    "\n",
    "Lambda functions are a concise way to create small, one-off functions in Python. They are defined using the lambda keyword and can have any number of arguments but only one expression.\n",
    "\n",
    "Lambda functions have the following syntax:\n",
    "```python\n",
    "lambda arguments: expression\n",
    "```\n",
    "\n",
    "As a trivial example, a Python function to add two numbers would be written as  -\n",
    "```python\n",
    "def add(x, y):\n",
    "  return x + y\n",
    "```\n",
    "As a lambda, this would be written as "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "649c1251-9cc9-43d8-938a-470119cca0e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_add = lambda x,y : x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aad176ea-86d5-4935-b55e-34c08cc09916",
   "metadata": {},
   "source": [
    "We could then call `my_add` as a function or pass `my_add` to yet another function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "5d5b7c3a-d9f3-4c4b-99c3-33a61255c43c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Calling directly: 7\n",
      "Passed to my_function: 22\n"
     ]
    }
   ],
   "source": [
    "print(\"Calling directly:\", my_add(5,2))\n",
    "\n",
    "def my_function(x,a,b):\n",
    "    print(\"Passed to my_function:\",x(a,b))\n",
    "\n",
    "my_function(my_add,10,12)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b05aa8a-3bb5-4688-a3a9-4fd0e5eeaf77",
   "metadata": {},
   "source": [
    "## Typical Usages\n",
    "\n",
    "- *In-Line Function Definition*: When a function is so simple that it can be defined in a single line. For example, defining a quick operation like a mathematical formula.\n",
    "- *Functional Programming*: Used extensively with functions like `map()`, `filter()`, and `reduce()` for performing operations on lists or other sequences. For instance, `map(lambda x: x*2, numbers)` to double each element in a list numbers.\n",
    "- *Functional Arguments*: When passing a simple function as an argument to another function, lambdas provide a concise way to define that function inline. Such examples include -\n",
    "  - *Sorting or Ordering Data*: Lambdas are often used in the key argument of sorting functions like sorted() or list.sort(), enabling custom sorting criteria. For example, sorting a list of tuples based on the second element.\n",
    "  - *Data Transformations*: In data science and analysis, particularly with libraries like Pandas, lambdas are used for applying quick transformations or calculations to columns in a DataFrame.\n",
    "  - *Event Handlers and Callbacks*: In GUI applications or in web frameworks, lambdas are used for defining simple event handlers or callbacks that execute in response to an event, like a button click.\n",
    "- *Temporary or Auxiliary Functions*: When a small utility function is needed temporarily, and there's no need to define a full function for it. This is common in scripting or automating small tasks.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "caa84bbb-a619-4b0b-aa08-536f57735e9c",
   "metadata": {},
   "source": [
    "## Limitations\n",
    "\n",
    "While lambda functions are usesful for their succinctness and convenience, they do have several limitations:\n",
    "- *Single Expression*: The most significant limitation is that they are restricted to a single expression. This means you cannot have multiple statements or complex logic within a lambda function.\n",
    "- *Readability*: For complex operations, lambdas can reduce readability. Python emphasizes readability, and sometimes a properly named regular function is more understandable and maintainable.\n",
    "- *No Statements*: Lambda functions cannot include statements like return, pass, assert, or annotations. They are purely functional in nature. You cannot have assignment statements in a lambda. This means you cannot set a variable inside a lambda function.\n",
    "- *Limited Exception Handling*: Since you cannot use statements, you can't handle exceptions within a lambda function using try-except blocks, which can be a significant drawback in error-prone operations.\n",
    "- *Limited Namespace Access*: Lambdas have limited access to variables that are not in their local scope or passed as arguments. While they can access variables in the outer scope, they cannot modify them.\n",
    "- *Lack of Documentation*: Unlike regular functions, lambdas do not support docstrings, so you cannot document a lambda function's behavior right where it's defined.\n",
    "- *No Recursion or Self-Reference*: Lambda functions cannot be recursive, i.e., they cannot call themselves, because they have no name to refer to themselves."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d65ad8f9-ea63-41db-b2a7-0405cd0d8925",
   "metadata": {},
   "source": [
    "## Examples\n",
    "Here's several examples of lambda's in use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "abf391c8-7c62-4900-aded-f11504dc258b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "25\n"
     ]
    }
   ],
   "source": [
    "# Square a number\n",
    "square = lambda x: x**2\n",
    "print(square(5))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "8ae7d918-97b9-4f21-bdf7-6ac64c1ab5a7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2, 4, 6, 8, 10]\n"
     ]
    }
   ],
   "source": [
    "# Filter even numbers\n",
    "numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]\n",
    "even_numbers = list(filter(lambda x: x % 2 == 0, numbers))\n",
    "print(even_numbers)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "dd546d2b-20d2-463d-bd0f-e6725b99e603",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[(5, 0), (3, 1), (1, 2), (4, 2)]\n"
     ]
    }
   ],
   "source": [
    "# Sort Tuples by the Second Item\n",
    "tuples = [(1, 2), (3, 1), (5, 0), (4, 2)]\n",
    "sorted_tuples = sorted(tuples, key=lambda x: x[1])\n",
    "print(sorted_tuples)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "91a27abf-d5f0-4a3e-873b-905900a59c17",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['Alice', 'Bob', 'Charlie']\n"
     ]
    }
   ],
   "source": [
    "# Capitalize Names\n",
    "names = ['alice', 'bob', 'charlie']\n",
    "capitalized_names = list(map(lambda name: name.capitalize(), names))\n",
    "print(capitalized_names)  # Output: ['Alice', 'Bob', 'Charlie']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "736022c2-f333-4710-be76-f742b20df045",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1102.5\n"
     ]
    }
   ],
   "source": [
    "# Calculate Compound Interest\n",
    "# Compound Intereset = Principal * (1 + Rate/100)^Time\n",
    "compound_interest = lambda principal, rate, time: principal * ((1 + rate/100) ** time)\n",
    "print(compound_interest(1000, 5, 2))  # Principal: $1000, Rate: 5%, Time: 2 years\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "479eae4a-ffa9-4c6c-b5b1-9d95211e2380",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.26\n"
     ]
    }
   ],
   "source": [
    "# Compute Annualized Return of an Investment\n",
    "# Annualized Return = ((Ending Value / Beginning Value) ^ (1 / Years)) - 1\n",
    "annualized_return = lambda beginning_value, ending_value, years: ((ending_value / beginning_value) ** (1 / years)) - 1\n",
    "print(\"{:.2f}\".format(annualized_return(1000, 2000, 3)))  # Investment doubling over 3 years"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a782ca9-63c1-42b9-a2e2-33c931f2f68c",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
