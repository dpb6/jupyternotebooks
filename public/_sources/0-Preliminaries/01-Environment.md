# Establishing Your Environment

A critical, initial step in any software development effort is to decide upon and establish the environment
used for the project. For many organizations, existing staff have made these decisions and you will have little choice. 
In other situations, you will have the freedom to decide which tools to use.  For FinTech 510, 
Programming for Financial Technology, and FinTech 512, Software Engineering, at [Duke University](https://duke.edu/), the
course instructors recommend using either the Windows or MacOS environments as specified in this section. While you are free
to use other tools, the course staff will only provide support these environments. 

## Command-line Tools
When possible, developers should use command-line tools due to their efficiency, versatility, and control. 
Command-line interfaces (CLIs) allow developers to perform tasks quickly and precisely through text-based commands, avoiding the 
need for navigating through graphical user interfaces (GUIs). Additionally, this streamlined approach enhances productivity 
by enabling automation, scripting, and batch processing; reducing repetitive tasks and minimizing human error. 
Command-line tools often provide more advanced functionalities and options compared to their GUI counterparts. For those not
used to interacting with the command-line, we encourage you to work through [Section 7, The Tools](../8-The%20Tools/1-Bash-Basics.md). MacOS provides a suitable environment although you will need to install the Command-line Tools package. For Windows, the situation is a little more complex. Over time, Microsoft has steadily moved the provided command-line system from [MS-DOS](https://en.wikipedia.org/wiki/MS-DOS) to [Powershell](https://en.wikipedia.org/wiki/PowerShell). As such, commands and provided programs have become more similar in nature to the Linux/Unix counterparts - however, there is still a substantial difference. As such we will utilize [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en-us/windows/wsl/about) for the command-line
environment.

## Programming Languages
For both platforms, the latest version of Pythons will be installed. For C++, the compiler support differs slightly between between the two platforms as well as between different OS versions. We will provide specific compiler flags to minimize these differences (e.g., to use C++ 17).  While we would prefer to use the
latest C++ standards, the C++ compiler included in [Ubuntu 24.04 LTS](https://ubuntu.com/) does not fully support the C++ 23 standard.

Note: We highly encourage students with an existing Python installation to use the environment defined in this guide.

```{admonition} Existing Conda or Anaconda Installations
If you have [Conda](https://conda.io) or [Anaconda](https://www.anaconda.com/), we recommend disabling this by default:

    (base) $ conda config --set auto_activate_base false
    (base) $ conda deactivate

If you need to use the environment, execute `conda activate`
```

## Guide
The installation will also install the source materials and environment used to create this guide.  We highly encourage
working through the notebooks, executing the content.  You should also make changes to the content (code blocks) to see what
happens as you do make changes. This interaction also serves to make you more engaged with the content rather than just being
a passive reader.

## VS Code 
Since its initial release in 2015, [Visual Studio Code (VSCode)](https://code.visualstudio.com/) has become an extremely popular development environment for many reasons:

- _Open Source:_ As an open-source product, VSCode is freely available for anyone to use, and the community can contribute to its development and create extensions, making it highly customizable and adaptable to different coding needs. [VSCode on GitHub](https://github.com/microsoft/vscode)
- _Cross-Platform:_ VSCode runs on Windows, macOS, and Linux, making it a versatile choice for developers who work on different operating systems.
- _Extensible_: Developers can add various extensions to tailor the editor to their specific needs, whether it's for a particular programming language, framework, or tool. While the Visual Studio Code Marketplace contains thousands of extensions, we will use just a few of the more popular ones for our development needs.
_Integrated Git:_ Git is a widely used version control system, and VSCode has excellent Git integration. Developers can easily manage version control, commit changes, and view Git history within the editor.
_Debugging:_ VSCode supports debugging for various programming languages and frameworks. It provides debugging tools, breakpoints, and a customizable debugging experience, which is crucial for troubleshooting and fixing code issues.
_Integrated Terminal:_ VSCode has an integrated terminal that allows developers to run command-line tools and scripts without leaving the editor, making the development workflow more seamless.
_Remote Development:_ VSCode allows developers to use their current computers to develop on remote systems and within Docker containers.  Windows users leverage this capability to develop in Windows Subsystem for Linux. _Remote development_ gives developers the full power of VSCode within environments typically limited to character-based terminals.

While other development environments are available (and terminal-based text editors such as [vim](https://www.vim.org/) and [GNU Emacs](https://www.gnu.org/software/emacs/) offer significant capabilities), this guide's recommendation is to use VSCode due to its capabilities and prevalence in financial organizations.

