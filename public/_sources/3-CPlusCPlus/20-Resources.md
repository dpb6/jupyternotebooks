# C++ Resources

Note: The book links direct users to the O'Reilly platform, but uses Duke University's access to that platform. Individuals not associated with the University will need to search for these resources.

## C++ Books
* [C++ Primer, 5th Ed](https://go.oreilly.com/duke-university/library/view/a-tour-of/9780136823575/)
* [C++20 for Programmers: An Objects-Natural Approach, 3rd Ed](https://go.oreilly.com/duke-university/library/view/c-20-for-programmers/9780136905776/)
* [Programming Principles and Practice using C++, 2nd Ed](https://go.oreilly.com/duke-university/library/view/programming-principles-and/9780133796759/)
* [Tour of C++, 3rd Ed](https://go.oreilly.com/duke-university/library/view/c-primer-fifth/9780133053043/)
* [Data Structures and Algorithms in C++, 2nd Ed](https://go.oreilly.com/duke-university/library/view/data-structures-and/9780470383278/)

## C++ Websites
* [https://www.learncpp.com/](https://www.learncpp.com/)
* [https://cppreference.com](https://cppreference.com/)
* [https://isocpp.org/](https://isocpp.org/)
* [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)

## C Books
* [The C Programming Language](https://go.oreilly.com/duke-university/library/view/~/9780133086249/?ar)
* [Practical C Programming](https://go.oreilly.com/duke-university/library/view/~/1565923065/?ar)
* [Effective C](http://go.oreilly.com/duke-university/library/view/~/9781098125677/?ar)
