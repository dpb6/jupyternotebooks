{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Object-Oriented Programming\n",
    "\n",
    "Object-oriented programming is a style of programming focused on objects. We create objects and tell them to do stuff by calling functions that belong to those objects. We typically refer to those object functions as methods. When two objects interact by calling methods and receiving return values, you may hear this called passing messages. The sending message is the method name and the associated arguments. The response message is the return value(s). With object-oriented programming, We reason about a program as a set of interacting objects rather than a set of actions(procedural programming).\n",
    "\n",
    "Objects are instances of a class. A class describes a type of object we might create. Classes correspond to nouns - for example, a general category of something like a bank account or a stock. Classes then define the fields (state) that the object will have. They also define the behavior by defining methods within the class definition. This behavior often can be found in the verbs used to discuss the particular problem domain. Methods declarations are just function definitions within a class declaration. A class forms a blueprint from which we create objects.\n",
    "\n",
    "As in Python, C++ classes provide similar abstraction capabilities to model something - i.e., the properties and behavior of something.  However, C++ provides greater capacity for information hiding.  Both languages support encapsulation, but with Python, programmers can peek in and directly manipulate the internal state of an object (one of Python's design guidelines is to trust to programmer to not perform maliciously).  C++ provides access control to properties and methods through the use of access specifiers - `public`, `private`, and `protected` - to control who can see which properties and methods. Instead of an initialize method (`__init__()`), C++ has constructors, which have the same name as the class.  Constructors do not have a return type specified as implicitly they return an object of the class type. Also notice that the 'self' argument present in Python classes is not needed in C++ as the methods implicitly have access to a pointer called `this` that points to the current object.  The methods `getY()` and `setY()` demonstrate using `this`. The type of `this` in a member function of class `X` is `X*` (pointer to `X`) [C++ Standard].\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining a Class in C++\n",
    "\n",
    "The following file demonstrates creating a class to represent an x,y coordinate.  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: simple_point.cpp\n",
    "//complile: g++ -std=c++17 -o simple_point simple_point.cpp\n",
    "//execute: ./simple_point\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "class Point {\n",
    "private:\n",
    "    double x;\n",
    "    double y;\n",
    "\n",
    "public:\n",
    "    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}\n",
    "\n",
    "    double getX() const {\n",
    "        return x;\n",
    "    }\n",
    "\n",
    "    void setX(double val) {\n",
    "        x = val;\n",
    "    }\n",
    "\n",
    "    double getY() const{\n",
    "        return this->y;\n",
    "    }\n",
    "\n",
    "    void setY(double val) {\n",
    "        this->y = val;\n",
    "    }\n",
    "};\n",
    "\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    Point p1;\n",
    "    Point p2(5,2);\n",
    "    Point p3(10);\n",
    "    p1.setY(10.0);\n",
    "\n",
    "    cout << \"p1: \" << p1.getX() << \",\" << p1.getY() << endl;\n",
    "    cout << \"p2: \" << p2.getX() << \",\" << p2.getY() << endl;\n",
    "    cout << \"p3: \" << p3.getX() << \",\" << p3.getY() << endl;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Line 7 starts the declaration of the class with the keyword `class` followed by the name of the class. As with Python, we could have created an empty class with `class empty {};`, but as C++ is statically typed, this declaration is useless.  Note that we have to end the class with a semi-colon in line 27.\n",
    "- Line 8 contains an _access specifier_ `private`. Until another access specifier appears, any methods or properties declared are considered \"private\" and only available for use within this class itself.  This provides information hiding (external entities do not have access to our internal implementation), which is a recommended practice with object-oriented programming.\n",
    "- Lines 9 and 10 declare two properties (attributes) of the class: x and y. These attributes are also referred to as data members.\n",
    "- Line 12 contains another _access specifier_, `public`.  Methods and properties in this section are available for use outside of the class.\n",
    "- Line 13 is a constructor to create a new instance (object) of the class `Point`. As with functions, we can provide provide default values.  After the colon `:`, is an initialization list that can set the object's properties.\n",
    "- Lines 15-17 and 23-25 are both _accessor_ methods to allow other code to access the values for x and y. The `const` keyword explicitly states to both the compiler and other developers. Variables accessed within these methods (i.e., the object's state) cannot be altered. \n",
    "- Lines 19-21 and 27-29 are _mutator_ methods used to allow other code to modify the object's state.\n",
    "- Within the main function, we declare and instantiate three point objects.   Notice that the constructor is automatically called in all three instances. `p1` uses both default values.  `p2` explicitly sets both `x` and `y`. `p3` sets `x`, but leaves the default value for `y`.  The code then modifies `p1`'s state to alter `y`.\n",
    "\n",
    "## Improving the Point Class\n",
    "The following four files demonstrate a more robust version of the `Point` class.\n",
    "\n",
    "Rather than having all of the source code in a single file, we now have it spread across three separate files. While this may seem complicated, this becomes necessary as we create larger systems. The point class is now divided into two files: `point.hpp `and `point.cpp`. The `point.hpp` contains the definition of the class. This header file will be included in whatever other source files (e.g., `main.cpp`) that will use the object. The point.cpp contains the implementation of the class. If you do not include the header file a statement such as \n",
    "\n",
    "```c++\n",
    "#include \"point.hpp\"\n",
    "```\n",
    "\n",
    "then the compiler will generate error messages as it does not know about the class `Point`.  The `main.cpp` file contains sample code using the Point class.\n",
    "\n",
    "To make building executables easier (as well as scripting other tasks such as cleaning files), we use a widely-adopted build automation tool, <a href=\"https://en.wikipedia.org/wiki/Make_(software)\">make</a>. When you execute \"make\" at the command-line, the tool looks for a configuration file called \"Makefile\". This file defines targets and then a series of commands to \"execute\"/\"complete\" that target.\n",
    "\n",
    "See the Makefile page in The Tools section for more information.\n",
    "\n",
    "`point.hpp`: Defines a header file that other classes can use via #include point.h at the top of the source file.  Notice this defines our state and then the interfaces (behavior) that other classes and source files can use.  We also use a preprocessor directive, `#ifndef` to test if the `POINT_H` macro has already been defined. If it has, then the code between `#ifndef` and `#endif` is skipped.  This is useful when we have complicated includes to prevent code from appearing more than once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: point.hpp\n",
    "//complile: g++ -std=c++17 -o point.o point.hpp\n",
    "//execute: not applicable - no main function \n",
    "#ifndef POINT_H\n",
    "#define POINT_H\n",
    "#include <iostream>\n",
    "\n",
    "class Point {\n",
    "private:\n",
    "  double x;\n",
    "  double y;\n",
    "\n",
    "public:\n",
    "  Point(double initialX=0.0, double initialY=0.0);\n",
    "  Point operator+(const Point& rhs) const;\n",
    "  double getX() const;\n",
    "  void setX(double val);\n",
    "  double getY() const;\n",
    "  void setY(double val);\n",
    "  void scale(double factor);\n",
    "  double distance(Point other) const; \n",
    "  void normalize();\n",
    "  Point operator+(Point other) const; \n",
    "  Point & operator+=(const Point &rhs);\n",
    "  Point operator*(double factor) const; \n",
    "  double operator*(Point other) const;\n",
    "};\n",
    "\n",
    "// Free-standing operator definitions, outside the formal Point class definition\n",
    "Point operator*(double factor, Point p);\n",
    "std::ostream& operator<<(std::ostream& out, Point p);\n",
    "#endif\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`point.cpp`: Implements the behavior for the `Point` class.\n",
    "\n",
    "In lines 44-60 - The class overloads the built-in operations for `+` and `*`. Notice also that we can either multiply by a factor or we can compute the dot product.  \n",
    "\n",
    "Lines 62-65 also overloads the `*`, but in this situation, we provide capabilities when the double is specified first in the expression.\n",
    "\n",
    "Lines 67-70 overload the `<<` operator. Note the return of the stream reference which supports chaining these method calls in an expression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: point.cpp\n",
    "//complile: g++ -std=c++17 -o point.o point.cpp\n",
    "//execute: not applicable - only builds an object file.\n",
    "#include \"point.hpp\"\n",
    "#include <cmath>     // for sqrt definition\n",
    "#include <iostream>  // for use of ostream\n",
    "using namespace std; // allows us to avoid qualified std::ostream syntax\n",
    "\n",
    "Point::Point(double initialX, double initialY) : x{initialX}, y{initialY} {}\n",
    "\n",
    "void Point::scale(double factor) {\n",
    "  x *= factor;\n",
    "  y *= factor;\n",
    "}\n",
    "\n",
    "double Point::getX() const {\n",
    "  return x;\n",
    "}\n",
    "\n",
    "void Point::setX(double val) {\n",
    "  x = val;\n",
    "}\n",
    "\n",
    "double Point::getY() const{\n",
    "  return y;\n",
    "}\n",
    "\n",
    "void Point::setY(double val) {\n",
    "  y = val;\n",
    "}\n",
    "\n",
    "double Point::distance(Point other) const {\n",
    "  double dx = x - other.x;\n",
    "  double dy = y - other.y;\n",
    "  return sqrt(dx * dx + dy *dy);\n",
    "}\n",
    "void Point::normalize() {\n",
    "  double mag = distance(Point());\n",
    "  if (mag > 0) {\n",
    "    scale(1 / mag);\n",
    "  }\n",
    "}\n",
    "\n",
    "Point Point::operator+(Point other) const {\n",
    "  return Point(x + other.x, y + other.y);\n",
    "}\n",
    "\n",
    "Point & Point::operator+=(const Point& rhs){ \n",
    "    this->x += rhs.getX();\n",
    "    this->y += rhs.getY();\n",
    "    return *this;\n",
    "}\n",
    "\n",
    "Point Point::operator*(double factor) const {\n",
    "  return Point(x * factor, y * factor);\n",
    "}\n",
    "\n",
    "double Point::operator*(Point other) const {\n",
    "  return x * other.x + y * other.y;\n",
    "}\n",
    "\n",
    "// Free−standing operator definitions, outside the formal Point class scope.    point p = factor * otherP\n",
    "Point operator*(double factor, Point p) {\n",
    "  return p * factor; // invoke existing form with Point as left operator\n",
    "}\n",
    "\n",
    "ostream &operator<<(ostream &out, Point p) {\n",
    "  out << \"<\" << p.getX() << \",\" << p.getY() << \">\";\n",
    "  return out;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`main.cpp`: Provides example code for using the `Point` class. Notice that in line 11, the uniform (brace) initialization syntax is used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "C++"
    }
   },
   "outputs": [],
   "source": [
    "//filename: main.cpp\n",
    "//complile: make\n",
    "//execute: ./point\n",
    "#include \"point.hpp\"\n",
    "#include <iostream>\n",
    "\n",
    "int main() {\n",
    "  std::cout << \"Hello World!\\n\";\n",
    "\n",
    "  Point a;                // Declares a, initalizes with the default constructor\n",
    "  Point b{5, 7};          // declares b, initilizes with a constructor\n",
    "  std::cout << \"Point a: \" << a << std::endl;\n",
    "  std::cout << \"Point b: \" << b << std::endl;\n",
    "\n",
    "  b.scale(2);           // calls method\n",
    "  std::cout << \"Point b(scaled): \" << b << std::endl;   //uses overload operator << on ostream\n",
    "  Point d = b * 2;       // declares d, assigns it as the result of b * 2.  * is defined in Point\n",
    "  std::cout << \"Point d(b*2): \" << d << std::endl;\n",
    " \n",
    "  Point e = 2 * b;       // uses the freestanding method.\n",
    "  std::cout << \"Point e(2*b): \" << e << std::endl;\n",
    "\n",
    "  e += b;       \n",
    "  std::cout << \"Point e += b: \" << e << std::endl;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Makefile`: Compiles each of the object files (main.o and point.o) and then combines them into the executable.  Also provides another target to remove the object files and any temporary files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "makefile"
    }
   },
   "outputs": [],
   "source": [
    "//filename: Makefile\n",
    "//Note - builds to executable\n",
    "//execute: make\n",
    "CPPFLAGS=-std=c++17 -pedantic -Wall -Werror -ggdb3\n",
    "point: main.o point.o\n",
    "        g++ -o point main.o point.o\n",
    "main.o: main.cpp\n",
    "        g++ $(CPPFLAGS) -c main.cpp\n",
    "point.o: point.cpp point.hpp\n",
    "        g++ $(CPPFLAGS) -c point.cpp\n",
    "\n",
    ".PHONY: clean\n",
    "clean:\n",
    "        rm -f *.o *~ point"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, in the terminal, execute \"make\" to build the executable. Then execute \"./point\" to run the code.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constructors\n",
    "\n",
    "Constructors are used to initialize the data members (properties) of a class object.  Whenever an object is created, a constructor will execute.\n",
    "\n",
    "Constructors have the same name as the class name, but they do not have a return type - implicitly their return type is just that of the class itself.  As with other functions, constructors can take zero or more parameters as well as specifying default values for parameters. Constructors may also be overloaded (i.e., we can have multiple constructors for a class) - they just must differ from each other by the parameter types and/or the number of parameters.\n",
    "\n",
    "The point class could have the following constructors:\n",
    "\n",
    "```c++\n",
    "Point() { x = 0.0; y = 0.0; }\n",
    "Point(double initialX, double initialY) : x{initialX}, y{initialY} {}\n",
    "Point(Point &p) { x = p.x; y = p.y; }\n",
    "```\n",
    "\n",
    "The first constructor is called the _default constructor_ as it takes no arguments. A constructor that provides default values for all of its parameters is also a default constructor. If a class does not explicitly define any constructors, the compiler implicitly defines a default constructor. This synthesized default constructor will initialize each data member (property) through calling a default constructor for that member unless it has been set to another value.  Primitive types such as int and double are initialized to zero. Generally speaking, your classes should have a default constructor as many of the C++ standard libraries depend upon these being present.\n",
    "\n",
    "In the second constructor, we utilize a `constructor initializer list` that specifies the initial values for one or more of the object's data members. The elements in this list are executed in order of how the members are defined for the class itself. To avoid confusion, you should follow the same order.  Any data member not initialized in this list follows the same logic/process as it would have under the synthesized default constructor. Using the initialize list is the preferred way to establish initial attribute values.  You can place more intricate initializations within the constructor body.\n",
    "\n",
    "The third constructor is a _copy constructor_. This constructor is used  to create a new constructor from an existing object.  The C++ compiler will automatically create a copy constructor if one is not already written. By default, this synthesized constructed will individually assign each data member in order.  Generally, this will not be a problem except when __an object owns pointers or non-shareable references, such as to a file.__ \n",
    "\n",
    "The copy constructor is called in the following situations: 1) an object of the class is returned by value from a method/function; 2) an object of the class is passed (to a method/function) by value as an argument; 3) an object is constructed based on another object of the same class; and 4) the compiler generates a temporary object.\n",
    "\n",
    "Whenever you write a copy constructor, you should also write a destructor and assignment operator.  \"<a href=\"https://en.wikipedia.org/wiki/Rule_of_three_(C%2B%2B_programming)\">Rule of Three</a>\".  Note: With C++ 11, the has expanded to the \"Rule of Five\" and covered in [Essential Class Operations](../16-EssentialClassOperations/16-EssentialClassOperations.ipynb).\n",
    "\n",
    "When an object contains pointers or references as data members, it will generally be necessary to write a copy constructor.  Generally, you will want to perform a deep-copy of the pointed to/referenced object.  By default, the synthesized copy constructor only performs a shallow-copy.  The semantics of the shallow vs deep copy are the same as those presented in the Python discussion.\n",
    "\n",
    "Note: It does not make sense (and is not allowed) for a constructor to be declared as `const`. The constructor's purpose is to initialize (change) the object.  An object cannot become immutable until after the construction (initialization) is complete.\n",
    "\n",
    "From a best-practice perspective, an object should be ready for use once the constructor is complete."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Destructor\n",
    "\n",
    "The _destructor_ performs whatever work is necessary to free resources used by an object (e.g., release dynamically allocated memory, close files) and then destroy data members themselves.\n",
    "\n",
    "```c++\n",
    "class Point {\n",
    "public:\n",
    "    ~Point() {   //destructor\n",
    "        //perform cleanup actions\n",
    "    }\n",
    "};\n",
    "```\n",
    "\n",
    "The ~_ClassName_ defines the destructor, which takes no arguments and has no return value. As a destructor takes no arguments, it cannot be overloaded.\n",
    "\n",
    "The destructor is called automatically whenever an object of its type is destroyed:\n",
    "\n",
    "- Variable goes out of scope.\n",
    "- Data members of an object when that object is destroyed.\n",
    "- Elements in a container (to be presented when we cover containers / STL) is destroyed.\n",
    "- When dynamically allocated object are destroyed through either the `delete` or `delete[]` operators.\n",
    "- When temporary objects are no longer needed.\n",
    "\n",
    "If pointers exist within an object, that memory is not automatically reclaimed (freed).\n",
    "\n",
    "The C++ compiler will create a _synthesized destructor_ for any class that does not define one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Copy-Assignment Operator\n",
    "\n",
    "The copy-assignment operator controls how objects of the class are assigned\n",
    "\n",
    "```c++\n",
    "Point p1(5,2), p2;\n",
    "p2 = p1;  //uses the Point copy-assignment operator\n",
    "```\n",
    "\n",
    "As with the copy constructor and destructor, the compile creates a synthesized copy-assignment operator for a class if it is not already defined. The synthesized constructor assigns each data member of the right-hand side object to the object's data members on the left-hand side.  The following code explicitly defines the copy-assignment operator for Point:\n",
    "\n",
    "```c++\n",
    "Point& operator=(const Point& other) {\n",
    "    x = other.x;\n",
    "    y = other.y;\n",
    "    return *this;\n",
    "}\n",
    "```\n",
    "\n",
    "In the last line, we want to return the Point itself, so we need to dereference `this` to refer to the actual object.\n",
    "\n",
    "As with the copy constructor and destructor, the copy-assignment operator needs to be written when dynamically allocated objects are present or with other allocated resources (e.g, file, network connection, etc.). Commonly, this is referred to as the \"<a href=\"https://en.wikipedia.org/wiki/Rule_of_three_(C%2B%2B_programming)\">Rule of Three</a>\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessors vs Mutators\n",
    "\n",
    "_Accessor_ methods are those methods that cannot alter the state of an object.  _Mutator_ methods are those methods that may alter an object's state. In C++, we explicitly designate accessor methods by placing the keyword `const` at the end of the function signature (i.e., after the parameters), but before the method's body.\n",
    "\n",
    "When the keyword `const` is placed before the type of the parameter, the function/method guarantees (and enforced by the compiler) that the object will not be modified. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Static\n",
    "\n",
    "As with Python, C++ supports the concept that classes may require members (both properties and methods) that belong to the class itself rather than instances of the class (objects). Static members are declared by placing the keyword static before that member's declaration.  Static members can be public, private, or protected as well as refer to any possible variable.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: account.cpp\n",
    "//complile: g++ -std=c++17 -o account.o account.cpp\n",
    "//execute: not applicable - only an object file is produced.\n",
    "class Account {\n",
    "public:\n",
    "    Account(double initialAmount): amount{initialAmount} {};\n",
    "    void calculate() { amount += amount * interestRate;}\n",
    "    static double getRate() { return interestRate;}\n",
    "    static void setRate(double newRate) { interestRate = newRate;}\n",
    "private:\n",
    "    double amount;\n",
    "    static double interestRate;\n",
    "};\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sample LLM Prompts\n",
    "1. Should I always use access methods in c++ or is it acceptable to directly access an object's variables(state)?\n",
    "1. Why is it important to separate the class declaration from the class definition in c++?  \n",
    "1. Explain C++'s rule of three as if I student in an introductory example.  Use a finance-related code sample."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "\n",
    "1. How do constructors and functions differ in C++?\n",
    "2. How is a defualt constructor identified? Does C++ always have a default constructor? When is the default constructor called?\n",
    "3. What actions should be performed within a destructor?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Acknowledgements: The Point class has been adopted from \"A Transition Guide from Python 2.x to C++\" by Michael Goldwasser and David Letscher."
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
