{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exceptions\n",
    "As introductory students developing programs for class assignments, validating input and terminating upon invalid input or terminating after detecting runtime errors usually suffices.  However, for robust, production-quality code, we need to detect, respond, and recover from a variety of errors. We start to develop robust programs through rigorous input validation, but that validation may not cover (or prevent) the various run-time errors that can occur.  For instance, if a user selects a given input file, we only know it is valid after we complete parsing/processing it. A number of issues may arise along the way and it's nearly impossible to prevent all such problems.\n",
    "Exceptions are run-time errors that occur outside of the normal functioning of a program.  We use exception handling when one part of a program detects a problem it cannot resolve. That part signals (by throwing an exception) that an issue has occurred.  Control will then pass to another part of the program that can handle that exception.  While typical examples will show the two parts co-located in a `try-catch `statement, the detecting error may occur within functions called by the statements in the `try` block. The exception basically jumps to the point in the call stack that can handle the handle - this creates a clean separation between error detection and error recovery/handling.\n",
    "Similar to Python, C++ provides exception handling to deal with unusual conditions during the runtime of a program in a controlled and systematic manner. When an exception occurs, the normal flow of the program is interrupted, and control is transferred to a special code block called a handler. Our goal in these handlers is to return the program to a valid state or to gracefully exit the program if that is impossible.\n",
    "Python and C++ have similar syntax for exception handling. C++ syntax -\n",
    "```c++\n",
    "try {\n",
    "  // code that may throw exception(s)\n",
    "} catch (const ExceptionType1 &ex1) {\n",
    "  // code to handle ExceptionType1\n",
    "} catch (const ExceptionType2 &ex2) {\n",
    "  // code to handle ExceptionType2\n",
    "} // … more catch blocks as needed\n",
    "catch (...) {\n",
    "  // Catch-all handler for other unhandled exceptions\n",
    "  std::cerr << \"Unknown exception caught\" << std::endl;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: divisionzero.cpp\n",
    "//complile: g++ -std=c++17 -o divisionzero divisionzero.cpp\n",
    "//execute: ./divisionzero\n",
    "#include <iostream>\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    try {\n",
    "        \n",
    "        int divisor = 0;\n",
    "        if (divisor == 0) {\n",
    "            throw std::runtime_error(\"Division by zero exception\");   // Note: throwing a value object here\n",
    "        }\n",
    "        int result = 10 / divisor;  // this statement never executes.  Creates a \"float-point\" exception, but outside of C++\n",
    "                                    // can not detect this error\n",
    "    }\n",
    "    catch (const std::runtime_error &e) { // note output to cout instead of cerr to display within docable.\n",
    "        std::cout << \"Runtime error: \" << e.what() <<  \"\\n\";\n",
    "        return EXIT_FAILURE;\n",
    "    } catch (...) {\n",
    "        std::cout << \"Unknown exception caught\" <<  \"\\n\";\n",
    "        return EXIT_FAILURE;\n",
    "    }\n",
    "    return EXIT_SUCCESS;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "As you can see in the example, when we detect an exceptional condition, we \"throw\" an exception with the `throw` keyword:\n",
    "```c++\n",
    "throw runtime_error(\"A problem occurred.\");\n",
    "```\n",
    "Here, `runtime_error` is a standard exception type provided by the C++ Standard Library. We can throw objects of any data type as exceptions, including built-in, custom, or library types. For example, the C++ STL throws an `out_of_range` exception in the `vector<>:at()` method if the provided index is invalid.\n",
    "\n",
    "One of the nuances when converting string values is that the parser will stop considering input when an invalid character is encountered.  As such, if you want to ensure that the entire string has been processed, we need to check how many characters were processed as compared to the length of the string. Note: With modern C++, we do not need to indicate that a function throws an exception - we do use the keyword `noexcept` if the function is guaranteed not throw an exception.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: convert.cpp\n",
    "//complile: g++ -std=c++17 -o convert convert.cpp\n",
    "//execute: ./convert\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "\n",
    "int convertInt(std::string s) {\n",
    "    std::size_t idx = 0;\n",
    "    int result = std::stoi(s,&idx);\n",
    "    if (idx != s.size()) {\n",
    "        throw std::invalid_argument(\"unprocessed input: \"+s);\n",
    "    }\n",
    "    return result; \n",
    "}\n",
    "\n",
    "int main() {\n",
    "    try {\n",
    "        std::cout << convertInt(\"100\") << \"\\n\";\n",
    "        std::cout << convertInt(\"100.4\") << \"\\n\";\n",
    "    } catch (const std::invalid_argument& a) {\n",
    "        std::cerr << \"Invalid argument: \" << a.what() << \"\\n\";\n",
    "        return EXIT_FAILURE;\n",
    "    } catch (const std::out_of_range& r) {\n",
    "        std::cerr << \"out of range of double: \" << r.what() << \"\\n\";\n",
    "        return EXIT_FAILURE;\n",
    "    }\n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exception Propagation\n",
    "If an exception is thrown but not caught in a particular scope, the exception propagates up to higher levels of the call stack until it is caught or until it reaches `main`. If it gets to `main` without being caught, the program will terminate. As the exception propagates through the call stack, those corresponding functions exit/go out of scope. Any declared objects within those functions will have their corresponding destructors called. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Standard Exceptions\n",
    "The C++ library defines several exceptions used to report issues within the library.  As from above, the library can detect these situations, but separate code must exist to handle and recover from these exceptions.  Here are some of these defined exceptions:\n",
    "| <b>Exception Class</b>|<b>Purpose</b> |\n",
    "|--|--|\n",
    "|`exception`|base class - most general kind of problem|\n",
    "|`runtime_error`|represents problems that can only be detected at runtime.|\n",
    "|`overflow_error`|Computation overflowed (number too great for the underlying type)|\n",
    "|`underflow_error`|occurs when a floating-point operation results in a value that is closer to zero than the smallest representable positive value for the data type being used, and the value cannot be represented accurately. |\n",
    "|`out_of_range`|thrown when an argument value is out of the valid range.|\n",
    "|`invalid_argument`|thrown when an invalid argument is passed to a function.|\n",
    "\n",
    "These classes are defined in `stdexcept` - you will need to include that header to be able to reference these classes. \n",
    "\n",
    "While these errors have been defined, C++ often leaves it to the program to detect these situations and throw the appropriate exception.\n",
    "\n",
    "## Notes\n",
    "- Do not throw exceptions from destructors, as this can cause unexpected behavior.  (Destructors will be covered in classes.)\n",
    "- Use exceptions for exceptional, non-routine error conditions.\n",
    "- Catch exceptions by reference (preferably `const` reference). This provides a number of benefits:\n",
    "  - allows us to use polymorphic behavior when accessing the exceptions (polymorphism will be covered later)\n",
    "  - signifies that will not change the exception object.\n",
    "  - prevents \"object slicing\" when a copy of the exception is made if the exception is not a reference. \n",
    "    Entering into a catch block functions similarly to calling a function.  Object slicing occurs when an \n",
    "    object of a subclass is assigned to an instance of a base class - we lose access to the state and \n",
    "    behavior defined in the subclass. The copy function in the base class only knows about its state, not those of any subclasses."
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
