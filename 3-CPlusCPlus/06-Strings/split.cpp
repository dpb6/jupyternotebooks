#include <iostream>
#include <string>
using namespace std;

int main() {
    string s = "java:c:c++:Python:Pascal:Basic";
    string delim = ":";

    auto start = 0U;
    auto end = s.find(delim);
    while (end != string::npos) {
        std::cout << s.substr(start, end - start) << "\n";
        start = end + delim.length();
        end = s.find(delim, start);
    }

    std::cout << s.substr(start, end) << "\n";
    std::cout << "Final value of end: " << end << "\n";
}