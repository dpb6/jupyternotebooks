{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Expressions and Operators\n",
    "_Expressions_ are any combination of one ore more literals, variables, function calls, and operators. An _operator_ is a keyword or a symbol thats used to perform an operation such as addition."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assignment\n",
    "An assignment statement calculates the value on the right side of equals statement, converts the value if necessary, and then replaces the object (memory location) of the left-hand side of the assignment with that value. A simple example follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: script0.cpp\n",
    "//complile: g++ -std=c++17 -o script0 script0.cpp\n",
    "//execute: ./script0\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int i = 20;\n",
    "    int j = 4;\n",
    "\n",
    "    cout << \"i: \" << i << \", j: \" << j << \"\\n\";\n",
    "\n",
    "    i = j;\n",
    "\n",
    "    cout << \"i: \" << i << \", j: \" << j << \"\\n\";\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Line 13 demonstrates the simple assignment. The value is read from the variable j and then written to i. As both variables are the same type, no conversion was necessary.\n",
    "The left operand in an assignment is typically referred to as an _lvalue_ in C++. The term originates from it being the left operand. However, you may want to think of it as standing for _locator value_ instead - the lvalue must designate an object (i.e., a location in memory). The right operand is always an expression and is referred to as an _rvalue_.\n",
    "```c++\n",
    "j = i + 15; \n",
    "```\n",
    "In the above line of code, the expression `i + 15` is an rvalue. The `i` (which can be an lvalue by itself) is converted into an rvalue as its value is read. The addition operation results in an rvalue that's then assigned to the lvalue of `j`.\n",
    "The following line of code demonstrates an error as an rvalue must always by on the right side of the assignment operator.\n",
    "```c++\n",
    "10 = i;  /* error: rvalue appears on the left-hand side of the assignment */\n",
    "```\n",
    "The assignment operation does return the rvalue in C++. Neither Python or Java return a value in an assignment statement/operation - this also prevents issues in boolean expressions and control statements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Increment and Decrement Operators\n",
    "Unlike Python, C++ contains unary increment(`++`) and decrement(`--`) operators that can upon an lvalue - these operations either add 1 to a value or subtract 1. They can be either prefix operators, in which the operation comes first and then the resulting value is returned; or, postfix operators in which the initial value is returned and then the operation occurs.\n",
    "```c++\n",
    "int i = 5;\n",
    "int result;\n",
    "result = i++; /* result contains 5, while i now has 6 */\n",
    "result = i--; /* result contains 6, while i now has 5 */\n",
    "result = ++i; /* result and i both contain 6 */\n",
    "result = --i; /* result and i both contain 5 */\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Arithmetic Operators\n",
    "The following table lists the arithmetic operators available in C++. Unlike Python, there is no floor division. You will need to use casts depending on the resulting value desired.\n",
    "| Operator|Meaning |\n",
    "|--|--|\n",
    "| *|Multiplication |\n",
    "| /|Division |\n",
    "| %|Remainder |\n",
    "| +|Addition (or unary plus) |\n",
    "| -|Subtraction (or unary minus) |\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: arithmetic.cpp\n",
    "//complile: g++ -std=c++17 -o arithmetic arithmetic.cpp\n",
    "//execute: ./arithmetic\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int a = 9,b = 4;\n",
    "    int result;\n",
    "    \n",
    "    result = a * b;\n",
    "    cout << \"a * b = \" << result << \"\\n\";\n",
    "\n",
    "    result = a / b;\n",
    "    cout << \"a / b = \" << result << \"\\n\";\n",
    "\n",
    "    cout << \"a / b = \" << (float) a / b << \"\\n\";   /* results in a float value */ \n",
    "\n",
    "    result = a % b;\n",
    "    cout << \"a % b = \" << result << \"\\n\";\n",
    "\n",
    "    cout << \"-3 % 9 = \" << -3 % 9 << \"\\n\";   /* NOTE: remainder, not modulo.  Python produces 6 */\n",
    "\n",
    "    result = a + b;\n",
    "    cout << \"a + b = \" << result << \"\\n\";\n",
    "\n",
    "    result = a - b;\n",
    "    cout << \"a - b = %\" << result << \"\\n\";\n",
    "    \n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Shorthand Assignment Operators\n",
    "Similar to Python, C++ supports the expected shorthand assignment operators:\n",
    "| Operator|Example|Equivalent to&nbsp; |\n",
    "|--|--|--|\n",
    "| *=|a *= b|a = a * b |\n",
    "| /=|a /= b|a = a / b |\n",
    "| %=|a %= b|a = a % b |\n",
    "| +=|a += b|a = a + b |\n",
    "| -=|a -= b|a = a - |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Relational Operators\n",
    "In C++, relation operators compare two operands. A C did not originally have a boolean type, these operators return 1 for true and 0 for false. Relational operators are primarily used when making decisions in selector (`if`, `switch`) and iteration (`while`, `for`) statements.\n",
    "| Operator|Meaning|Example |\n",
    "|--|--|--|\n",
    "| ==|equals|5 == 5 |\n",
    "| !=|not equals|5 != 6 |\n",
    "| &gt;|greater than|6 &gt; 5 |\n",
    "| &gt;=|greater than or equals|4 &gt;= 4 |\n",
    "| &lt;|less than|4 &lt; 5 |\n",
    "| &lt;=|less than or equals|3 &lt;= 3 |\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: relation.cpp\n",
    "//complile: g++ -std=c++17 -o relation relation.cpp\n",
    "//execute: ./relation\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "using std::endl;\n",
    "\n",
    "int main() {\n",
    "    int a = 11, b = 5, c = 11;\n",
    "\n",
    "    cout << a << \" == \" << b << \" is \" << (int) (a == b) << \"\\n\";\n",
    "    cout << a << \" == \" << c << \" is \" << (int) (a == c) << \"\\n\";\n",
    "    cout << a << \" > \"  << b << \" is \" << (int) (a > b) << \"\\n\";\n",
    "    cout << a << \" > \"  << c << \" is \" << (int) (a > c) << \"\\n\";\n",
    "    cout << a << \" < \"  << b << \" is \" << (int) (a < b) << \"\\n\";\n",
    "    cout << a << \" < \"  << c << \" is \" << (int) (a < c) << \"\\n\";\n",
    "    cout << a << \" != \" << b << \" is \" << (int) (a != b) << \"\\n\";\n",
    "    cout << a << \" != \" << c << \" is \" << (int) (a != c) << \"\\n\";\n",
    "    cout << a << \" >= \" << b << \" is \" << (int) (a >= b) << \"\\n\";\n",
    "    cout << a << \" >= \" << c << \" is \" << (int) (a >= c) << \"\\n\";\n",
    "    cout << a << \" <= \" << b << \" is \" << (int) (a <= b) << \"\\n\";\n",
    "    cout << a << \" <= \" << c << \" is \" << (int) (a <= c) << \"\\n\";\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Logical Operators\n",
    "C++ supports the same set of logical operators as Python, including short-circuit decisions. However rather than using the keywords of `and`, `or`, and `not`, C++ uses the following symbols as operators:\n",
    "| Operator|Alternative Spelling|Meaning|Example |\n",
    "|--|--|--|--|\n",
    "| &amp;&amp;| and | Logical AND.&nbsp; True only both operands are true.|int i = 5int k = 5k == 5 &amp;&amp; i == 4 evaluates to false |\n",
    "| \\|\\|| or | logical OR. True when one or both of the operands are true.|k == 5 || i == 4 evaluates to true |\n",
    "| !| not | Logical NOT.&nbsp; True only if the operand is zero (0).|!k evaluates to false |\n",
    "\n",
    "Note: The C++ standard does allow alternative spellings for these operators."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: logic.cpp\n",
    "//complile: g++ -std=c++17 -o logic logic.cpp\n",
    "//execute: ./logic\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "using std::endl;\n",
    "\n",
    "int main() {\n",
    "    int a = 11, b = 5, c = 11, result;\n",
    "\n",
    "    result = (a == b) && (c > b);\n",
    "    cout << \"(a == b) && (c > b) is \" << result << \"\\n\";\n",
    "\n",
    "    result = (a == b) && (c < b);\n",
    "    cout << \"(a == b) && (c < b) is \" << result << \"\\n\";\n",
    "\n",
    "    result = (a == b) || (c < b);\n",
    "    cout << \"(a == b) || (c < b) is \" << result << \"\\n\";\n",
    "\n",
    "    result = (a != b) || (c < b);\n",
    "    cout << \"(a != b) || (c < b) is \" << result << \"\\n\";\n",
    "\n",
    "    result = !(a != b);\n",
    "    cout << \"!(a != b) is  \" << result << \"\\n\";\n",
    "\n",
    "    result = !(a == b);\n",
    "    cout << \"!(a == b) is  \" << result << \"\\n\";\n",
    "\n",
    "    return 0;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bitwise Operators\n",
    "C++ has a series of operators to support bit-wise operations. Typically, these are performed on integer types. Short-hand assignment operators do exist for these as well.\n",
    "| Operator|Meaning|Example |\n",
    "|--|--|--|\n",
    "| &amp;|AND operator.&nbsp; bit has to exist in both operands| |\n",
    "| \\||OR. Bit exists in either operand| |\n",
    "| ^|XOR. Bit is set if the bit is set in one operand, but not both| |\n",
    "| ~|Binary complement| |\n",
    "| &lt;&lt;|Shift left operator| |\n",
    "| &gt;&gt;&nbsp;|Shift right operator| |\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: bitwise.cpp\n",
    "//complile: g++ -std=c++17 -o bitwise bitwise.cpp\n",
    "//execute: ./bitwise\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "using std::endl;\n",
    "\n",
    "\n",
    "std::string toBinaryString(int number) {\n",
    "    if (number == 0) { return \"0\"; }\n",
    "\n",
    "    std::string binary = \"\";\n",
    "    while (number > 0) {\n",
    "        binary = std::to_string(number % 2) + binary; // Add remainder at the front of string\n",
    "        number /= 2; \n",
    "    }\n",
    "\n",
    "    return binary;\n",
    "}\n",
    "\n",
    "void pBinary(int number, int num_digits) {\n",
    "    std::string binary = toBinaryString(number);\n",
    "    while (binary.length() < num_digits) {\n",
    "        binary = \"0\" + binary;\n",
    "    }\n",
    "    cout << binary;\n",
    "}\n",
    "\n",
    "int main() {\n",
    "    unsigned int a = 8;\n",
    "    unsigned int b = 8;\n",
    "    unsigned int c = 7;\n",
    "    unsigned int d = 1;\n",
    "\n",
    "    pBinary(a,4); cout << \" & \"; pBinary(a,4); cout << \" = \"; pBinary(a&b,4); cout << \"\\n\";\n",
    "    pBinary(a,4); cout << \" & \"; pBinary(c,4); cout << \" = \"; pBinary(a&c,4); cout << \"\\n\";\n",
    "    pBinary(a,4); cout << \" | \"; pBinary(c,4); cout << \" = \"; pBinary(a|c,4); cout << \"\\n\";\n",
    "    pBinary(c,4); cout << \" ^ \"; pBinary(d,4); cout << \" = \"; pBinary(c^d,4); cout << \"\\n\";\n",
    "    cout << \"~\"; pBinary(c,4); cout << \" = \"; pBinary(~c,4); cout << \"\\n\";\n",
    "    pBinary(d,4); cout << \" << 3\";  cout << \" = \"; pBinary(d << 3,4); cout << \"\\n\";\n",
    "    pBinary(a,4); cout << \" >> 1\";  cout << \" = \"; pBinary(a >> 1,4); cout << \"\\n\";\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operator Precedence\n",
    "Operator precedence determines how an expression is evaluated. C++ and Python follow largely the same rules. Use parenthesis whenever there might be any confusion on the ordering of operations.\n",
    "<table style=\"text-align:center;\" class=\"table table-bordered\">\n",
    "<tbody><tr> \n",
    "<th style=\"text-align:center;\">Category</th> \n",
    "<th style=\"text-align:center;\">Operator</th>\n",
    "<th style=\"text-align:center;\">Associativity</th> \n",
    "</tr> \n",
    "<tr> \n",
    "<td>Postfix</td>\n",
    "<td>() [] -&gt; . ++ - -</td> \n",
    "<td>Left to right</td> \n",
    "</tr>\n",
    "<tr> \n",
    "<td>Unary</td> \n",
    "<td>+  -  !  ~  ++  - -  (type)*  &amp; sizeof</td> \n",
    "<td>Right to left</td>\n",
    "</tr> \n",
    "<tr> \n",
    "<td>Multiplicative</td> \n",
    "<td>*  /  %</td>\n",
    "<td>Left to right</td> \n",
    "</tr> \n",
    "<tr> \n",
    "<td>Additive</td>\n",
    "<td>+  -</td> \n",
    "<td>Left to right</td> \n",
    "</tr>\n",
    "<tr> \n",
    "<td>Shift</td> \n",
    "<td>&lt;&lt; &gt;&gt;</td> \n",
    "<td>Left to right</td> \n",
    "</tr> \n",
    "<tr> \n",
    "<td>Relational</td>\n",
    "<td>&lt; &lt;=  &gt; &gt;=</td> \n",
    "<td>Left to right</td> \n",
    "</tr>\n",
    "<tr> \n",
    "<td>Equality</td> \n",
    "<td>==  !=</td> \n",
    "<td>Left to right</td> \n",
    "</tr> \n",
    "<tr> \n",
    "<td>Bitwise AND</td>\n",
    "<td>&amp;</td> \n",
    "<td>Left to right</td> \n",
    "</tr> \n",
    "<tr> \n",
    "<td>Bitwise XOR</td> \n",
    "<td>^</td> \n",
    "<td>Left to right</td>\n",
    "</tr> \n",
    "<tr> \n",
    "<td>Bitwise OR</td> \n",
    "<td>|</td> \n",
    "<td>Left to right</td>\n",
    "</tr> \n",
    "<tr> \n",
    "<td>Logical AND</td>\n",
    "<td>&amp;&amp;</td> \n",
    "<td>Left to right</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td>Logical OR</td> \n",
    "<td>||</td> \n",
    "<td>Left to right</td>\n",
    "</tr> \n",
    "<tr> \n",
    "<td>Conditional</td>\n",
    "<td>?:</td> \n",
    "<td>Right to left</td> \n",
    "</tr>\n",
    "<tr>\n",
    "<td>Assignment</td> \n",
    "<td>=  +=  -=  *=  /=  %=&gt;&gt;=  &lt;&lt;=  &amp;=  ^= |=</td>\n",
    "<td>Right to left</td>\n",
    "</tr>\n",
    "<tr> \n",
    "<td>Comma</td> \n",
    "<td>,</td> \n",
    "<td>Left to right</td>\n",
    "</tr> \n",
    "</tbody></table>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Logical and Bitwise Operators\n",
    "While it may seem feasible to interchange the use of `&&` and `&`, you should avoid this practice. Use the logical operator (`&&`) when comparing two expressions and testing that they both evaluate to true. Use the bitwise operator (`&`) when comparing/performing bit-level operations. Don't mix and match these operators - C++ will let you, but does it make semantic sense?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: logical_bit.cpp\n",
    "//complile: g++ -std=c++17 -o logical_bit logical_bit.cpp\n",
    "//execute: ./logical_bit\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "using std::endl;\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int i = 2;\n",
    "    int j = 4;\n",
    "    if (i && j) {\n",
    "        cout << \"logical and: true, value \" << (int) (i && j) << \"\\n\";\n",
    "    }\n",
    "    if (i & j) {\n",
    "        cout << \"bitwise and: true \" << \"\\n\";\n",
    "    }\n",
    "    else {\n",
    "        cout << \"bitwise and: false, value \" << (int) (i & j) << \"\\n\";\n",
    "    }\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### char Arithmetic\n",
    "In Python, we did not have the ability to manipulate a character value - technically it was a string with just 1 character - we had to convert it to a number with the `ord()` function. In C++, the `char` data type is an integer type, typically of just a single character. As such we can directly perform arithmetic on the variables without performing any conversions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: char.cpp\n",
    "//complile: g++ -std=c++17 -o char char.cpp\n",
    "//execute: ./char\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "using std::endl;\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    char c = 'a';   /* 97 comes from the ASCII value for 'a' */\n",
    "    c += 5;\n",
    "\n",
    "    cout << \"original value as int: \" << (int) 'a' << \"\\n\";\n",
    "    cout << \"Value after addition as int: \" << (int) c << \"\\n\";\n",
    "    cout << \"Value after addition as char: \"<< c << \"\\n\";\n",
    "\n",
    "    return 0;\n",
    "}\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
