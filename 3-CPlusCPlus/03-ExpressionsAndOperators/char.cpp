#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    char c = 'a';   /* 97 comes from the ASCII value for 'a' */
    c += 5;

    cout << "original value as int: " << (int) 'a' << "\n";
    cout << "Value after addition as int: " << (int) c << "\n";
    cout << "Value after addition as char: "<< c << "\n";

    return 0;
}