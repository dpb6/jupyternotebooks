#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    int i = 2;
    int j = 4;
    if (i && j) {
        cout << "logical and: true, value " << (int) (i && j) << "\n";
    }
    if (i & j) {
        cout << "bitwise and: true " << "\n";
    }
    else {
        cout << "bitwise and: false, value " << (int) (i & j) << "\n";
    }
    return 0;
}