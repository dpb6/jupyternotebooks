#include "point.hpp"
#include <cmath>     // for sqrt definition
#include <iostream>  // for use of ostream
using namespace std; // allows us to avoid qualified std::ostream syntax

Point::Point(double initialX, double initialY) : x{initialX}, y{initialY} {}

void Point::scale(double factor) {
  x *= factor;
  y *= factor;
}

double Point::getX() const {
  return x;
}

void Point::setX(double val) {
  x = val;
}

double Point::getY() const{
  return y;
}

void Point::setY(double val) {
  y = val;
}

double Point::distance(Point other) const {
  double dx = x - other.x;
  double dy = y - other.y;
  return sqrt(dx * dx + dy *dy);
}
void Point::normalize() {
  double mag = distance(Point());
  if (mag > 0) {
    scale(1 / mag);
  }
}

Point Point::operator+(Point other) const {
  return Point(x + other.x, y + other.y);
}

Point & Point::operator+=(const Point& rhs){ 
    this->x += rhs.getX();
    this->y += rhs.getY();
    return *this;
}

Point Point::operator*(double factor) const {
  return Point(x * factor, y * factor);
}

double Point::operator*(Point other) const {
  return x * other.x + y * other.y;
}

// Free−standing operator definitions, outside the formal Point class scope.    point p = factor * otherP
Point operator*(double factor, Point p) {
  return p * factor; // invoke existing form with Point as left operator
}

ostream &operator<<(ostream &out, Point p) {
  out << "<" << p.getX() << "," << p.getY() << ">";
  return out;
}
