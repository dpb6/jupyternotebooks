#include <iostream>
using namespace std;

class Point {
private:
    double x;
    double y;

public:
    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}

    double getX() const {
        return x;
    }

    void setX(double val) {
        x = val;
    }

    double getY() const{
        return this->y;
    }

    void setY(double val) {
        this->y = val;
    }
};


int main(int argc, char *argv[]) {
    Point p1;
    Point p2(5,2);
    Point p3(10);
    p1.setY(10.0);

    cout << "p1: " << p1.getX() << "," << p1.getY() << endl;
    cout << "p2: " << p2.getX() << "," << p2.getY() << endl;
    cout << "p3: " << p3.getX() << "," << p3.getY() << endl;
}