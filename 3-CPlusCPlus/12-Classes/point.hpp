#ifndef POINT_H
#define POINT_H
#include <iostream>

class Point {
private:
  double x;
  double y;

public:
  Point(double initialX=0.0, double initialY=0.0);
  Point operator+(const Point& rhs) const;
  double getX() const;
  void setX(double val);
  double getY() const;
  void setY(double val);
  void scale(double factor);
  double distance(Point other) const; 
  void normalize();
  Point operator+(Point other) const; 
  Point & operator+=(const Point &rhs);
  Point operator*(double factor) const; 
  double operator*(Point other) const;
};

// Free-standing operator definitions, outside the formal Point class definition
Point operator*(double factor, Point p);
std::ostream& operator<<(std::ostream& out, Point p);
#endif