#ifndef LIBRARYSYSTEM_H
#define LIBRARYSYSTEM_H

#include <string>

class LibraryItem {
public:
    LibraryItem(const std::string& title, const std::string& authorship, int publicationYear)
        : title(title), authorship(authorship), publicationYear(publicationYear) {}

    virtual void displayDetails() const = 0;

protected:
    std::string title;
    std::string authorship;
    int publicationYear;

    virtual ~LibraryItem() = 0;
};

LibraryItem::~LibraryItem() {}

// Abstract class for digital assets
class DigitalAsset {
public:
    DigitalAsset(double fileSize, const std::string& fileFormat)
        : fileSize(fileSize), fileFormat(fileFormat) {}

    void displayDetails() const {
        std::cout << "File Size (MB): " << fileSize <<"\n";
        std::cout << "File Format: " << fileFormat << "\n";
    }


protected:  // Make protected to allow derived classes access
    double fileSize;
    std::string fileFormat;

    virtual ~DigitalAsset() = 0;
};

DigitalAsset::~DigitalAsset() {}

// Derived class for books
class Book : public LibraryItem {
public:
    Book(const std::string& title, const std::string& author, int publicationYear, 
         const std::string& genre, int pageNumber)
        : LibraryItem(title, author, publicationYear), genre(genre), pageNumber(pageNumber) {}

    void displayDetails() const {
        std::cout << "Book Title: " << title << "\n";
        std::cout << "Author: " << authorship << "\n";
        std::cout << "Year: " << publicationYear << ", Genre: " << genre << ", Pages:" << pageNumber << "\n";
    }

private:
    std::string genre;
    int pageNumber;
protected:
    virtual ~Book() = 0;

};
Book::~Book() {}

class Movie : public LibraryItem {
public:
    Movie(const std::string& title, const std::string& director, int publicationYear, 
          const std::string& genre, int duration)
        : LibraryItem(title, director, publicationYear), genre(genre), duration(duration) {}

    void displayDetails() const {
        std::cout << "Movie: " << title << "\n";
        std::cout << "Directory: " << authorship << "\n";
        std::cout << "Year Released: " << publicationYear << ", Genre: " << genre << ", Duration(min):" << duration << "\n";
    }
private:
    std::string genre;
    int duration;
    
protected:
    virtual ~Movie() = 0;
};
Movie::~Movie() {}

class EBook : public Book, public DigitalAsset {
public:
    EBook(const std::string& title, const std::string& author, int publicationYear, 
          const std::string& genre, int pageNumber,
          double fileSize, const std::string& fileFormat)
        : Book(title, author, publicationYear, genre, pageNumber), 
          DigitalAsset(fileSize, fileFormat) {}

    ~EBook() {} 
    
    void displayDetails() const {
        Book::displayDetails();
        DigitalAsset::displayDetails();
    }

};


class PrintedBook : public Book {
public:
    PrintedBook(const std::string& title, const std::string& author, int publicationYear, 
                const std::string& genre, int pageNumber, 
                const std::string& coverType, double weight)
        : Book(title, author, publicationYear, genre, pageNumber), 
               coverType(coverType), weight(weight) {}
    ~PrintedBook() {} 

    void displayDetails() const {
        Book::displayDetails();
        std::cout << "CoverType: " << coverType << "\n";
    }

private:
    std::string coverType;
    double weight;
};


class DigitalMovie : public Movie, public DigitalAsset {
public:
    DigitalMovie(const std::string& title, const std::string& director, int publicationYear, 
                 const std::string& genre, int duration, 
                 double fileSize, const std::string& fileFormat)
        : Movie(title, director, publicationYear, genre, duration), 
          DigitalAsset(fileSize, fileFormat) {}

    ~DigitalMovie() {} 

    void displayDetails() const {
        Movie::displayDetails();
        DigitalAsset::displayDetails();
    }
};


class BluRay : public Movie {
public:
    BluRay(const std::string& title, const std::string& director, int publicationYear, 
           const std::string& genre, int duration, 
           const std::string& resolution)
        : Movie(title, director, publicationYear, genre, duration), resolution(resolution) {}
    ~BluRay() {} 

    void displayDetails() const {
        Movie::displayDetails();
        std::cout << "Resolution: " << resolution << "\n";
    }
private:
    std::string resolution;
};


class DVD : public Movie {
public:
    DVD(const std::string& title, const std::string& director, int publicationYear, 
        const std::string& genre, int duration, int regionCode)
        : Movie(title, director, publicationYear, genre, duration), regionCode(regionCode) {}

    ~DVD() {} 

    void displayDetails() const {
        Movie::displayDetails();
        std::cout << "RegionCode: " << regionCode << "\n";
    }

private:
    int regionCode;
};

#endif // LIBRARYSYSTEM_H
