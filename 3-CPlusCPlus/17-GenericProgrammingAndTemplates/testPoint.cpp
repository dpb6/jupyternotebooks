#include "point.hpp"
#include <iostream>
#include <string>

int main(int argc, char *argv[]) {
    Point<double> dp(1.0,3.14);
    std::cout << dp.toString() << std::endl;

    Point<int> ip(1,5);
    std::cout << ip.toString() << std::endl;

    Point<char> cp('A','D');
    std::cout << cp.toString() << std::endl;

    Point<int> p(10,3);
    std::cout << ip.distance(p) << std::endl;

}