{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generic Programming and Templates\n",
    "\n",
    "In Python, we could easily write functions that dealt with a variety of different input types without knowing those exact types as we wrote our code.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "//filename: script0.py\n",
    "//execute: python script0.py\n",
    "def min(a,b):\n",
    "    if (a < b):\n",
    "        return a\n",
    "    else:\n",
    "        return b\n",
    "\n",
    "print(min(5, 3))\n",
    "print(min(2.72, 3.14))\n",
    "print(min(\"world\",\"hello\"))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "With C++'s static typing, we need to explicitly define those types. \n",
    "\n",
    "The idea behind generic programming is to have templated functions and classes in which the logic is written independently of any particular type. As we use templated code, we then supply the appropriate types and the compiler instantiates functions and/or classes specific to the specified type(s). Effectively, these templates serve as blueprints and we still have the advantage of well-defined types.\n",
    "\n",
    "## Templated Functions\n",
    "\n",
    "In the following code, we define a templated function min that works for a variety of different types as long as that type (class) implements the `&lt;` operator.\n",
    "\n",
    "In this example, we declare a placeholder for the type name immediately before the function definition: `template &lt;typename t&gt;`\n",
    "\n",
    "```c++\n",
    "template<typename T>\n",
    "returnType functionName(parameterList){\n",
    "    //function body\n",
    "}\n",
    "```\n",
    "\n",
    "T can be any type.  We can use other placeholder names than T and have multiple \"substitution\" types (just separate by commas)\n",
    "\n",
    "If the compiler can determine the type(s) for the templated function, we do not need to explicitly state the type (line 16). However, in the case of line 17 as the two arguments differ in type, we need explicitly tell the compiler which type to using within the myMin function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: min_template.cpp\n",
    "//complile: g++ -std=c++17 -o min_template min_template.cpp\n",
    "//execute: ./min_template\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "using namespace std;\n",
    "\n",
    "template <typename T>\n",
    "T myMin(T a, T b) {\n",
    "    if (a < b) {\n",
    "        return a;\n",
    "    }\n",
    "    else {\n",
    "        return b;\n",
    "    }\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    cout << myMin(1.0,0.5) << endl;\n",
    "    cout << myMin<int>(42, 37.4) << endl;\n",
    "\n",
    "    // use the C++ string class\n",
    "    string s1(\"Jane\");\n",
    "    string s2(\"John\");\n",
    "    cout << myMin(s1, s2) << endl;\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Templated Classes\n",
    "\n",
    "Templated classes operate much the same way templated functions do. We specify the type substitutions ahead of the class definition instead of the function defintion.\n",
    "\n",
    "```c++\n",
    "template<typename T>\n",
    "class ClassName {\n",
    "private:\n",
    "    //private members\n",
    "public:\n",
    "    //public members\n",
    "}\n",
    "```\n",
    "\n",
    "As the entire class must be available for the compiler to instantiate a new class definition where the templated classes are instantiated, the entire body of the class is typically defined in header files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: point.hpp\n",
    "//complile: g++ -std=c++17 -o point point.hpp\n",
    "//execute: ./point\n",
    "#include <string>\n",
    "#include <cmath>\n",
    "using std::string;\n",
    "\n",
    "#ifndef POINT_TEMPLATE_H_\n",
    "#define POINT_TEMPLATE_H_\n",
    "\n",
    "template <typename T>\n",
    "class Point {\n",
    "private:\n",
    "    T x;\n",
    "    T y;\n",
    "\n",
    "public:\n",
    "    Point(T initialX, T initialY) : x{initialX}, y{initialY} {}\n",
    "\n",
    "    T getX() const {\n",
    "        return x;\n",
    "    }\n",
    "\n",
    "    void setX(T val) {\n",
    "        x = val;\n",
    "    }\n",
    "\n",
    "    T getY() const{\n",
    "        return this->y;\n",
    "    }\n",
    "\n",
    "    void setY(T val) {\n",
    "        this->y = val;\n",
    "    }\n",
    "\n",
    "    T distance(Point other) const {\n",
    "        T dx = x - other.x;\n",
    "        T dy = y - other.y;\n",
    "        return sqrt(dx * dx + dy *dy);\n",
    "}\n",
    "\n",
    "    string toString() {\n",
    "        return \"(\"+std::to_string(x)+\",\"+std::to_string(y)+\")\";\n",
    "    }\n",
    "};\n",
    "\n",
    "#endif\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The follow main method demonstrates using the templated Point class.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: testPoint.cpp\n",
    "//complile: g++ -std=c++17 -o testPoint testPoint.cpp\n",
    "//execute: ./testPoint\n",
    "#include \"point.hpp\"\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    Point<double> dp(1.0,3.14);\n",
    "    std::cout << dp.toString() << std::endl;\n",
    "\n",
    "    Point<int> ip(1,5);\n",
    "    std::cout << ip.toString() << std::endl;\n",
    "\n",
    "    Point<char> cp('A','D');\n",
    "    std::cout << cp.toString() << std::endl;\n",
    "\n",
    "    Point<int> p(10,3);\n",
    "    std::cout << ip.distance(p) << std::endl;\n",
    "\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notes:\n",
    "\n",
    "- Templated classes and functions are only type-checked once they are instantiated by the compiler. If the underlying type does not support operators or other function arguments than a compilation error will occur.\n",
    "- With functions, template parameters may be inferred.  However, with classes, the template parameters must always be supplied.  e.g., `Point x(1,2)` produces a compilation error.\n",
    "- Note: instead of `template<typename T>`, `template<class T>` can be used. The two statements have the same semantic meaning. While Bjarne Stroustrup states in _Programming: Principles and Practices Using C++_, \"We are of the opinion that class already means type, so it makes no difference. Also, class is shorter.\", this statement leaves much to be desired.  In C++, class actually means a user-defined type and is separate from the primitive types.  Prefer the keyword `typename` to  avoid confusion."
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
