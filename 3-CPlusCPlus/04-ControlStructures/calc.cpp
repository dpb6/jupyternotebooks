#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    char operation;
    double n1, n2;

    cout << "Enter a math operation such as 5 + 2: ";
    std::cin >> n1 >> operation >> n2;
    if (std::cin.fail()) {             // Checks in the input succeeded or not ..
        std::cerr << "Failed to read the math operation" << "\n";
        return EXIT_FAILURE;
    } 

    switch(operation) {
        case '+':
            cout << n1 << " + " << n2 << " = " << n1 + n2 << "\n";
            break;

        case '-':
            cout << n1 << " - " << n2 << " = " << n1 - n2 << "\n";
            break;

        case '*':
            cout << n1 << " * " << n2 << " = " << n1 * n2 << "\n";
            break;

        case '/':
            cout << n1 << " / " << n2 << " = " << n1 / n2 << "\n";
            break;

        // operator doesn't match any case constant +, -, *, /
        default:
            std::cerr << "Error! operator is not correct: " << operation << "\n";
    }

    return EXIT_SUCCESS;
}