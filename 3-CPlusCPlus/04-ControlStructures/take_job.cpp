#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    int salary = 60000;
    int commute_time_min = 30;
    int free_coffee = 0;

    if (salary > 50000) {
        if (commute_time_min < 60) {
            if (free_coffee) {
                cout << "Cool.  I'm going to like this job.\n";
            } else {
                cout << "Close location and I like the salary, but I NEED coffee.\n";
            }
        } else {
            cout << "I don't want to drive that far.\n";
        }
    } else {
        cout << "I'm worth more than that.\n";
    }
}