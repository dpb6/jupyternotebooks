#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
    vector<string> lines;
    lines.push_back("It was the best of times.");
    lines.push_back("It was the worst of times.");
    lines.push_back("It was the age of wisdom");
    lines.push_back("It was the age of foolishness");
    
    for (auto line: lines) {
        cout << line << endl;
    }    
    return EXIT_SUCCESS;
}
