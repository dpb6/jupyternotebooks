#include <iostream>
#include <stdexcept>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

bool compareDescending(int a, int b) {
    return a > b; 
}

int main(int argc, char *argv[]) {
    //setup a uniform random distribution between 0 and 10,000: https://en.cppreference.com/w/cpp/numeric/random
    double lower_bound = 0;
    double upper_bound = 10000;
    std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
    std::default_random_engine re;

    vector<double> numbers;
    for (int i=0; i < 100; i++) {
        numbers.push_back(unif(re));
    }

    //cout << min_element(numbers.begin(), numbers.end());

    cout << "total: " <<  accumulate(numbers.begin(), numbers.end(), 0.0) << "\n";
    cout << "min: " <<  *min_element(numbers.begin(), numbers.end()) << "\n";      // returns a pointer to the element
    cout << "max: " <<  *max_element(numbers.begin(), numbers.end()) << "\n";

    sort(numbers.begin(), numbers.end(), compareDescending);
    cout << "sorted: ";
    bool passed_first = false;
    for (auto a: numbers) {
        if (passed_first) { cout << ","; }
        else {passed_first = true;}
        cout << a;
    }
    cout << "\n";

    return EXIT_SUCCESS;
}