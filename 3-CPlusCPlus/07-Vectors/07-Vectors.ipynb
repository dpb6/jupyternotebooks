{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Vectors\n",
    "One of the advantages Python offers is the many built-in data structures - lists, dictionaries, and many others.C++ offers the Standard Template Library (STL). While the class explores the STL library more in-depth later, we'll first start with the `vector` class that provides similar functionality to Python's list, but with the requirement that only objects of a single type can be stored within it. Behind the scenes, `vector` is a dynamic array implementation that stores elements sequentially. Arrays are contiguous sequences of memory that hold objects of a common type. Unlike regular arrays, vectors can grow and shrink their memory footprint and track their actual size.\n",
    "\n",
    "The program below demonstrates using the `vector` class. In line 11, we declare (and initialize) an empty vector `items` that holds values of type `string`. The `< >` syntax identifies a template in C++. We can use different types to create vectors that hold different types such as `double` and `int`. While we learn how to create template types later, just realize that you can use any type there."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: vector.cpp\n",
    "//complile: g++ -std=c++17 -o vector vector.cpp\n",
    "//execute: ./vector\n",
    "#include <iostream>\n",
    "#include <vector>              // Include the vector header\n",
    "#include <string>\n",
    "#include <algorithm>           // Will use to sort the list\n",
    "\n",
    "using std::cin, std::cout;  // bring in these names from the std namespace\n",
    "using std::string, std::vector;        \n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    string item;\n",
    "    vector<string> items;    // creates an empty vector;\n",
    "\n",
    "    cout << \"Enter a series of strings to add to the vector, ctrl-d to stop: \";\n",
    "    while (cin >> item) {\n",
    "        items.push_back(item);\n",
    "    }\n",
    "\n",
    "    cout << \"Number of items: \" << items.size() << \"\\n\";\n",
    "\n",
    "    std::sort(items.begin(), items.end());         // begin() and end() are iterators - see below\n",
    "\n",
    "    cout << \"Iterate over the list with a for-each loop :\" <<  \"\\n\";\n",
    "    for (string s: items) {\n",
    "        cout << s <<  \"\\n\";\n",
    "    }\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Declare and Initialize\n",
    "C++ provides several different ways to declare and initialize vectors:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: declare.cpp\n",
    "//complile: g++ -std=c++17 -o declare declare.cpp\n",
    "//execute: ./declare\n",
    "#include <vector> \n",
    "using std::vector; \n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int n = 4;\n",
    "    vector<int> vec;                         // empty vector holding objects of type int.\n",
    "    vector<int> five{1, 2, 3, 4, 5};         // vector contains as many elements as there are \n",
    "                                             // initializers (values with { } on the right-hand side)\n",
    "    vector<int> alt_five = {1, 2, 3, 4, 5};  // equivalent to the above declaration\n",
    "    vector<int> another(alt_five);           // another has a copy of each element of alt_five\n",
    "    vector<int> v1(n);                       // vector has n copies of a value-initialized object.\n",
    "                                             // (would be the default constructor or zero for numbers)\n",
    "    vector<int> v2(n,5);                     // holds n copies of the value 5.\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While it may be tempting to immediately create a vector of a specific size if you know that size, you need to be wary that behind the scenes, C++ constructs objects and places them into the vector. Instead, you should declare the vector, and then call `reserve` with the appropriate size. The vector library, though, is very efficient at growing. The following code demonstrates creating a vector filled with random numbers and shows how the capacity doubles as additional memory space is needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: random.cpp\n",
    "//complile: g++ -std=c++17 -o random random.cpp\n",
    "//execute: ./random\n",
    "#include <random>\n",
    "#include <iostream>\n",
    "#include <vector>    \n",
    "#include <string>\n",
    "using std::cout, std::vector;  \n",
    "\n",
    "int main() {\n",
    "    std::random_device rd;  // Random engine\n",
    "    std::mt19937 eng(rd());   // Mersenne Twister engine seeded with rd().  Can fix the seed with an int\n",
    "    \n",
    "    // Create an object that produces random numbers from the Uniform distribution between 1 and 1000\n",
    "    std::uniform_int_distribution<> distr(1, 1000);  \n",
    "\n",
    "    int random_integer = distr(eng);\n",
    "    cout << \"Random number: \" << random_integer <<  \"\\n\";\n",
    "\n",
    "    vector<int> items; \n",
    "    int capacity_increases = 0;\n",
    "    int last_capacity = 0; \n",
    "    for (int i=1; i < 1000; i++) {\n",
    "        items.push_back( distr(eng));  // add a random number to the vector\n",
    "        if (items.capacity() != last_capacity) { // check if the vector capacity increases\n",
    "            capacity_increases++;\n",
    "            last_capacity = items.capacity();\n",
    "            cout << i << \" \" << last_capacity <<  \"\\n\";\n",
    "        }\n",
    "    }\n",
    "    cout << \"Total number of increases: \" << capacity_increases <<  \"\\n\";\n",
    "\n",
    "    return 0;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Accessing Elements\n",
    "Use either the `[]` operator or the `at()` method to access elements.\n",
    "\n",
    "The [] operator is slightly faster as it does not perform bounds checking. However, this could lead to undefined behavior when accessing memory not appropriately initialized. This method is preferred when the boundary sizes are known (which should be almost always). The `at()` method performs bounds checking and will throw a `std::out_of_range` exception if you attempt to access an element at an invalid index. This allows you to handle the error gracefully using a try-catch block.\n",
    "\n",
    "C++ also provides special methods `front()` and `back()` to access the ends of the vector. We could also explicitly use 0 and `size()-1`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: access.cpp\n",
    "//complile: g++ -std=c++17 -o access access.cpp\n",
    "//execute: ./access\n",
    "#include <vector> \n",
    "#include <iostream>\n",
    "using std::vector; \n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    vector<int> vec{1, 2, 3, 4, 5}; \n",
    "\n",
    "    // note the use of size_t as the type for i: https://en.cppreference.com/w/cpp/types/size_t\n",
    "    for (size_t i=0; i < vec.size(); i++) {\n",
    "        std::cout << vec[i] << \" - \" << vec.at(i) <<  \"\\n\";\n",
    "    } \n",
    "\n",
    "    //DANGER - demonstrating how the [] does not perform bounds checking.\n",
    "    std::cout << vec[10] << std::endl;\n",
    "\n",
    "    // Using at() - With bounds checking\n",
    "    try {\n",
    "        std::cout << vec.at(7) << std::endl;    // throws an exception\n",
    "    } catch(const std::out_of_range& e) {\n",
    "        std::cout << \"Out of Range error: \" << e.what() <<  \"\\n\";\n",
    "    }\n",
    "\n",
    "    std::cout << \"First element: \" << vec.front() << \" -- \" << vec[0] <<  \"\\n\";\n",
    "    std::cout << \"Last element: \" << vec.back() << \" -- \" << vec[vec.size()-1]  <<  \"\\n\";\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Adding and Removing Elements\n",
    "C++ provides several methods to add and remove elements. We've already demonstrated `push_back()` which adds an element to the end of the list. `pop_back()` removes an element from the end of a list, but does not return a value. __`insert(_pos_, _element_)`__inserts an element at the specified position. Rather than requiring an integral value, we'll need to use an iterator value - which we can access with either `begin()` or `end()`. Then we can use arithmetic to get to the correct position. `erase(_pos_)` removes an element at the specified position. You can also specify a range of elements to remove. We'll need to provide two iterators to `erase()` function: the iterator pointing to the first element to be removed and the iterator pointing to one past the last element to be removed - that is, a range of elements `[first, last)`. `clear()` removes all elements from the vector.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: insert.cpp\n",
    "//complile: g++ -std=c++17 -o insert insert.cpp\n",
    "//execute: ./insert\n",
    "#include <vector> \n",
    "#include <iostream>\n",
    "using std::vector; \n",
    "\n",
    "// Defining a templated function so that we can print vectors of any type\n",
    "template<typename T>\n",
    "void printVector(const std::vector<T>& vec) {\n",
    "    std::cout << \"{ \";\n",
    "    bool first = true;\n",
    "    for (const auto& elem : vec) {   // This is a fencepost loop.  Special case the first\n",
    "        if (first) {\n",
    "            std::cout << elem;\n",
    "            first = false;\n",
    "        }\n",
    "        else {\n",
    "            std::cout << \", \" << elem;\n",
    "        }\n",
    "    }\n",
    "    std::cout << \" }\" <<  \"\\n\";\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    vector<int> vec{1, 2,  4, 5}; \n",
    "\n",
    "    vec.insert(vec.begin() +2 ,3);\n",
    "    vec.insert(vec.end(),8);\n",
    "    vec.insert(vec.end()-1,7);   // what happens if you try vec.end() + 1 \n",
    "    printVector(vec);\n",
    "\n",
    "    // Insert multiple copies of an element\n",
    "    vec.insert(vec.begin(), 2, 0); // Insert two '0's at the start\n",
    "    printVector(vec);\n",
    "\n",
    "    // remove the first element\n",
    "    vec.erase(vec.begin());\n",
    "    printVector(vec);\n",
    "\n",
    "    // remove the last two elements\n",
    "    vec.erase(vec.end()-2,vec.end());\n",
    "    printVector(vec);\n",
    "\n",
    "    vec.clear();\n",
    "    printVector(vec);\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Size and Capacity\n",
    "Unlike using native arrays directly, we can obtain the current size (number of elements currently in the vector) and the capacity (the number of elements the vector can store without having to allocate more memory)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: capacity.cpp\n",
    "//complile: g++ -std=c++17 -o capacity capacity.cpp\n",
    "//execute: ./capacity\n",
    "#include <vector> \n",
    "#include <iostream>\n",
    "using std::vector; \n",
    "\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    vector<int> vec{1, 2, 3};\n",
    "\n",
    "    std::cout << \"Initial size: \" << vec.size() <<  \"\\n\";\n",
    "    std::cout << \"Initial capacity: \" << vec.capacity() <<  \"\\n\";\n",
    "\n",
    "    vec.push_back(4);  // note the doubling of the capacity\n",
    "    std::cout << \"Size: \" << vec.size() <<  \"\\n\";\n",
    "    std::cout << \"Capacity: \" << vec.capacity() <<  \"\\n\";\n",
    "} "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While not shown in the above code, both `size()` and `capacity()` generally return an unsigned long as the result type. (Technically, the exact type is `vector<int>::size_type`.) While in many cases you can use an `int`, depending upon compiler options, this may cause a warning and/or error. Generally, you should use `size_t` or `vector<elementType>::size_type` as the type. This page demonstrates this in the first loop under \"Accessing Elements\". [https://en.cppreference.com/w/cpp/types/size_t](https://en.cppreference.com/w/cpp/types/size_t)\n",
    "\n",
    "```c++\n",
    "    // note the use of size_t as the type for i\n",
    "    for (size_t i=0; i < vec.size(); i++) {\n",
    "        std::cout << vec[i] << \" - \" << vec.at(i) <<  \"\\n\";\n",
    "    } \n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Iterators\n",
    "This page has already demonstrated iterating through a vector using a for-each loop as well as a standard for loop with indexes. We presented iterator variables as we added and removed items with `insert()` and `erase()`. These iterators give us indirect access to an object - an element in a container such as a `vector`. We can use an iterator to fetch an element by using the dereference operator, `*`, from the specified location in the iterator variable. Unsurprisingly, we can use these variables to move through a vector. We can use arithmetic to move forward and backward in the vector with these iterator variables. Use `begin()` and `end()` to perform necessary bounds checking. A valid iterator signifies an element or a position just beyond the container’s last element. All other iterator values are invalid.\n",
    "Note the use of the `auto` keyword to define the iterator type. Except in extremely rare circumstances, knowing the exact type is unnecessary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: iterate.cpp\n",
    "//complile: g++ -std=c++17 -o iterate iterate.cpp\n",
    "//execute: ./iterate\n",
    "#include <vector> \n",
    "#include <iostream>\n",
    "using std::vector; \n",
    "\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    vector<int> vec{1, 2, 3};\n",
    "\n",
    "    for (auto it = vec.begin(); it != vec.end(); it++) {\n",
    "        std::cout << *it << ' ';\n",
    "    }\n",
    "    for (auto it = vec.end(); it >= vec.begin(); it--) { // note the comparison\n",
    "        std::cout << *it << ' ';\n",
    "    }\n",
    "    std::cout << std::endl;\n",
    "\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Memory Management\n",
    "One of the great things about using `vector `is not having to worry about memory. The class makes sensible decisions for us. However, based on specialized tasks (such as declaring a large vector that we know we'll use or freeing up space from a large vector that's no longer being used), we can call specific functions to reserve memory or to free memory.\n",
    "- `resize(n)` changes the size of the vector to `n`.\n",
    "- `reserve(n)` reserves at least `n` elements worth of memory.\n",
    "- `shrink_to_fit()` reduces memory usage by freeing unused space.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: memory.cpp\n",
    "//complile: g++ -std=c++17 -o memory memory.cpp\n",
    "//execute: ./memory\n",
    "#include <vector> \n",
    "#include <iostream>\n",
    "using std::vector; \n",
    "\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    vector<int> vec{1, 2, 3};\n",
    "\n",
    "    std::cout << \"1 - size: \" << vec.size() << \", capacity: \" << vec.capacity() <<  \"\\n\";\n",
    "\n",
    "    vec.reserve(10);  // increase capacity to 10\n",
    "    std::cout << \"2 - size: \" << vec.size() << \", capacity: \" << vec.capacity() <<  \"\\n\";\n",
    "\n",
    "    vec.push_back(4);\n",
    "    vec.shrink_to_fit();\n",
    "    std::cout << \"3 - size: \" << vec.size() << \", capacity: \" << vec.capacity() <<  \"\\n\";\n",
    "\n",
    "    vec.resize(2); // truncates to just the first two elements\n",
    "    std::cout << \"4 - size: \" << vec.size() << \", capacity: \" << vec.capacity() <<  \"\\n\";\n",
    "\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Combining functions, strings, and vectors\n",
    "Now that we have covered many of the basics in C++, we can start to develop useful, reusable functions.\n",
    "Below, we take the split code from the string docable and then encapsulate it into a function. Within `main`, we've written a small series of test cases to validate our function. You should comment lines 23 and 25 (so the last string is always added to the result) to see how things change."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: parseLine.cpp\n",
    "//complile: g++ -std=c++17 -o parseLine parseLine.cpp\n",
    "//execute: ./parseLine\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "#include <vector>\n",
    "\n",
    "/**\n",
    " * @brief Split apart a line based upon delimiters in the string\n",
    " * \n",
    " * @param line The string to be split\n",
    " * @param delim a string specifying the delimeter used to split apart the string\n",
    " * @return a vector of string objects\n",
    " */\n",
    "std::vector<std::string> splitLine(std::string line, std::string delim) {\n",
    "    std::vector<std::string> result;\n",
    "\n",
    "    auto start = 0U;\n",
    "    auto end = line.find(delim);\n",
    "    while (end != std::string::npos) {\n",
    "        result.push_back(line.substr(start, end - start));\n",
    "        start = end + delim.length();\n",
    "        end = line.find(delim, start);\n",
    "    }\n",
    "    std::string last = line.substr(start, end);\n",
    "    if (last.length()) {\n",
    "        result.push_back(last);\n",
    "    }\n",
    "\n",
    "    return result;\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    std::vector<std::string> samples = {\"\", \"java:c:c++:Python:Pascal:Basic\", \"C++\", \"C++:Java:\"};\n",
    "    std::vector<int> expected_sizes = {0,6,1,2};\n",
    "    std::string delim = \":\";\n",
    "\n",
    "    for (size_t i = 0U; i< samples.size(); i++) {\n",
    "        std::string line = samples[i];\n",
    "        std::cout << \"Testing: \" << line << std::endl;\n",
    "        std::vector<std::string> splits = splitLine(line,delim);\n",
    "        for (std::string item: splits) {\n",
    "            std::cout << item <<  \"\\n\";\n",
    "        }\n",
    "        if (splits.size() == expected_sizes[i]) {\n",
    "            std::cout << \"result size matched\" <<  \"\\n\";\n",
    "        }\n",
    "        else {\n",
    "            std::cout << \"ERROR - result size unexpected, expected: \" << expected_sizes[i] <<  \"\\n\";\n",
    "        }\n",
    "        std::cout << \"Finished: \" << line <<  \"\\n\";E\n",
    "    }\n",
    "    \n",
    "}"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
