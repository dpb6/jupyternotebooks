#include <vector> 
#include <iostream>
using std::vector; 

int main(int argc, char *argv[]) {
    vector<int> vec{1, 2, 3, 4, 5}; 

    // note the use of size_t as the type for i: https://en.cppreference.com/w/cpp/types/size_t
    for (size_t i=0; i < vec.size(); i++) {
        std::cout << vec[i] << " - " << vec.at(i) <<  "\n";
    } 

    //DANGER - demonstrating how the [] does not perform bounds checking.
    std::cout << vec[10] << std::endl;

    // Using at() - With bounds checking
    try {
        std::cout << vec.at(7) << std::endl;    // throws an exception
    } catch(const std::out_of_range& e) {
        std::cout << "Out of Range error: " << e.what() <<  "\n";
    }

    std::cout << "First element: " << vec.front() << " -- " << vec[0] <<  "\n";
    std::cout << "Last element: " << vec.back() << " -- " << vec[vec.size()-1]  <<  "\n";
}