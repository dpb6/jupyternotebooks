#include <iostream>

void f(double param[], size_t param_length) {
    std::cout << "size of param: " << sizeof(param) << ".  Address: " << param << "\n";
}

int main(int argc, char *argv[]) {
    double data[30];
    std::cout << "data array size: " << sizeof(data) << ".  Address: " << data << "\n";
    f(data,30);

    return EXIT_SUCCESS;
}