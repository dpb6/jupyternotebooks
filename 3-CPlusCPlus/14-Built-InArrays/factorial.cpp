#include <iostream>

 int main(int argc, char *argv[] ) {
     int factorial[7] = {1};

     for (int i=1; i < 7; i++) {
         factorial[i] = i * factorial[i-1];
     }
                
    for (int i = 0; i < 7; i++) {
        std::cout << "Factorial[" << i << "] = " << factorial[i] << "\n";
    }

    return EXIT_SUCCESS;
 }