#include <stdio.h>
#include <stdlib.h>

typedef struct _point point;
struct _point {
    int x;
    int y;
};

typedef struct _rect rect;
struct _rect {
    point pt1;
    point pt2;
};

typedef struct _circle circle;
struct _circle {
    point center;
    float radius;
};


int main(int argc, char *argv[]) {
    circle c = { {2,1}, 5};
    printf("(%d,%d) - area: %f\n", c.center.x, c.center.y, c.radius * c.radius * 3.14);

    point a = {6,6};
    point b = {3, -6};
    rect r;
    r.pt1 = a;
    r.pt2 = b;
    printf("(%d,%d) (%d,%d), area: %d\n", r.pt1.x, r.pt1.y, r.pt2.x, r.pt2.y, abs(r.pt1.x - r.pt2.x) * abs(r.pt1.y - r.pt2.y)   );


    return 0;
}