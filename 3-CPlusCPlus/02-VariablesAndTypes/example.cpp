#include <iostream>

int main(int argc, char *argv[]) {
    char a_lower = 'a';
    char a_upper = 'A';
    
    std::cout << a_lower << " " << (int) a_lower << "\n";
    std::cout << a_upper << " " << (int) a_upper << "\n";
    a_lower += 5;
    std::cout << a_lower << " " << (int) a_lower <<"\n";

    int c = 97;
    int f = c * 1.8 + 32;
    std::cout << c << "  Celsius to " << f << " Fahrenheit" << ", " << c * 1.8 + 32 << "\n";

    return 0;
}