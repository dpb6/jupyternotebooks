{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Variables and Types\n",
    "Note: This document contains very specific details - the intention is not for you to memorize these details but to be aware of them and how they can affect your programs.\n",
    "\n",
    "Generally speaking, we'll use a subset of these types as we programm in C++."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variables\n",
    "While potentially confusing to developers with experience in object-oriented languages (e.g., Python), C and C++ define the term _object_ as \"a region of data storage in the execution environment, the contents of which can represent values\". (This has been present since the C89 ANSI Standard). The C Programming Language book defines an _object_ \"(sometimes called a variable) as a location in storage, and its interpretation depends upon two main attributes: its storage class and its type. The storage class determines the lifetime of the storage associated with the identified object; the type determines the meaning of the values found in the identified object. A name also has a scope, which is the region of the program in which it is known\". We'll discuss types throughout the rest of this page, while storage class and scope will be discussed in the function notebook. C++ also uses the term _object_ as an instance of a class, also to be discussed later.\n",
    "A variable is a name that refers to a particular data storage location in the computer's memory.  Unlike Python, variables must be declared prior to their use in C++. In both languages, variables have an associated type that gives meaning to the data. Unlike Python, C++ variables cannot change types once defined. Everything is an object in Python with state and behavior, while in C++, only variables declared from classes have behavior.\n",
    "The following code example declares four variables in C++:\n",
    "```c++\n",
    "int i = 4;\n",
    "double d = 2.3;\n",
    "char c = 'h';\n",
    "int j;\n",
    "```\n",
    "\n",
    "C++ variable naming rules are very similar to those in Python.  A variable name can consist of alphabetical characters (lower and upper case), numbers, and the underscore character.  Names may not start with a number. Names may not be a C++ [keyword](https://en.cppreference.com/w/cpp/keyword).  By convention, variable names use camelCase rather than separating words by underscores. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Types\n",
    "\n",
    "Within C++, we can divide the available types into several different categories:\n",
    "1. Boolean\n",
    "2. Basic/Arithmetic Types: char, integer, and floating-point \n",
    "3. Enumerated Types: integer types, but assigned discrete values\n",
    "4. void - indicates no value is assigned\n",
    "5. Strings\n",
    "6. Classes / Structures\n",
    "7. Pointers (presented later)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Boolean\n",
    "\n",
    "Unlike Python, C++ did not originally have a Boolean data type until it's initial standardization in 1998.\n",
    "\n",
    "```c++\n",
    "bool t = true;\n",
    "bool f = false;\n",
    "```\n",
    "Note that the keywords for true and false are both lower case. Boolean values are also typically represented as integers where 0 represents false and any non-zero value represents true. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Arithmetic Types\n",
    "\n",
    "#### Char\n",
    "A `char` type represents a single character.  Typically this is a signed value and is mapped to the [ASCII values](https://en.wikipedia.org/wiki/ASCII). They can be assigned through a character literal that is represented by single quotes. Within C++, we can perform arithmetic on this data type.\n",
    "\n",
    "```c++\n",
    "char c = 'A';\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Integer Types\n",
    "Unlike Python, C++ contains a number of different integer types.  Additionally, these types can be defined as signed or unsigned.  Additionally, integers have limits in C++, unlike their counterparts within Python.\n",
    "\n",
    "| Type  | Storage Size | Value Range |\n",
    "|-----|----|-----|\n",
    "|char | 1 byte | -128 to 127 |\n",
    "|unsigned char | 1 byte | 0 to 255 |\n",
    "|signed char | 1 byte | -128 to 127 |\n",
    "|int | 4 bytes | -2,147,483,648 to 2,147,483,647 |\n",
    "|unsigned int | 4 bytes | 0 to 4,294,967,295 |\n",
    "|short | 2 bytes | -32,768 to 32,767 |\n",
    "|unsigned short | 2 bytes | 0 to 65535 |\n",
    "|long | 8 bytes | -9223372036854775808 to 9223372036854775807 |\n",
    "|unsigned long | 8 bytes | 0 to 18446744073709551615 |\n",
    "\n",
    "Unfortunately, with C++, these limits can vary based on the platform. The following program shows how we can find the specific limits.  Also, notice how we can use the `sizeof`` operator to get the number of bytes for a particular type (line 45).  `digits`` provides the number of digits the variable type can represent without a sign."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: integers.cpp\n",
    "//compile: g++ -std=c++17 -o integers integers.cpp\n",
    "//execute: ./integers\n",
    "#include <iostream>\n",
    "#include <limits>\n",
    "\n",
    "int main() {\n",
    "    std::cout << \"char:\\n\" <<\n",
    "                 \"  bits: \"   << std::numeric_limits<char>::digits      << \"\\n\" <<\n",
    "                 \"  signed: \" << std::numeric_limits<char>::is_signed   << \"\\n\" <<\n",
    "                 \"  min: \"    << (int) std::numeric_limits<char>::min() << \"\\n\" <<\n",
    "                 \"  max: \"    << (int) std::numeric_limits<char>::max() << \"\\n\";\n",
    "\n",
    "    // the cast to an int type was necessary as C++ would have displayed the min/max\n",
    "    // char instead\n",
    "\n",
    "    std::cout << \"int:\\n\" <<\n",
    "                 \"  bits: \"   << std::numeric_limits<int>::digits    << \"\\n\" <<\n",
    "                 \"  signed: \" << std::numeric_limits<int>::is_signed << \"\\n\" <<\n",
    "                 \"  min: \"    << std::numeric_limits<int>::min()     << \"\\n\" <<\n",
    "                 \"  max: \"    << std::numeric_limits<int>::max()     << \"\\n\";                 \n",
    "\n",
    "    std::cout << \"long:\\n\" <<\n",
    "                 \"  bits: \"   << std::numeric_limits<long>::digits    << \"\\n\" <<\n",
    "                 \"  signed: \" << std::numeric_limits<long>::is_signed << \"\\n\" <<\n",
    "                 \"  min: \"    << std::numeric_limits<long>::min()     << \"\\n\" <<\n",
    "                 \"  max: \"    << std::numeric_limits<long>::max()     << \"\\n\";                 \n",
    "\n",
    "    std::cout << \"short:\\n\" <<\n",
    "                 \"  bits: \"   << std::numeric_limits<short>::digits    << \"\\n\" <<\n",
    "                 \"  signed: \" << std::numeric_limits<short>::is_signed << \"\\n\" <<\n",
    "                 \"  min: \"    << std::numeric_limits<short>::min()     << \"\\n\" <<\n",
    "                 \"  max: \"    << std::numeric_limits<short>::max()     << \"\\n\";      \n",
    "\n",
    "    std::cout << \"unsigned char:\\n\" <<\n",
    "                 \"  bits: \"   << std::numeric_limits<unsigned char>::digits      << \"\\n\" <<\n",
    "                 \"  signed: \" << std::numeric_limits<unsigned char>::is_signed   << \"\\n\" <<\n",
    "                 \"  min: \"    << (int) std::numeric_limits<unsigned char>::min() << \"\\n\" <<\n",
    "                 \"  max: \"    << (int) std::numeric_limits<unsigned char>::max() << \"\\n\";                   \n",
    "\n",
    "   std::cout << \"unsigned int:\\n\" <<\n",
    "                 \"  bits: \"   << std::numeric_limits<unsigned int>::digits    << \"\\n\" <<\n",
    "                 \"  signed: \" << std::numeric_limits<unsigned int>::is_signed << \"\\n\" <<\n",
    "                 \"  min: \"    << std::numeric_limits<unsigned int>::min()     << \"\\n\" <<\n",
    "                 \"  max: \"    << std::numeric_limits<unsigned int>::max()     << \"\\n\";                   \n",
    "\n",
    "    int i = 5;\n",
    "    std::cout << \"int size (bytes): \" << sizeof(i) << \"\\n\";\n",
    "    \n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Code Example\n",
    "The following programming first declares and initializes two char variables.  Notice that in C++, we represent a single char value with single quotes ' . Within C++, it is important that you properly initialize variables before their use - by default, we don't know what value they represent otherwise. In the next section of the code, we declare two int values and convert the Celsius value to its corresponding Fahrenheit value. Note that the formula produces a double due to the `1.8` in the expression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: example.cpp\n",
    "//compile: g++ -std=c++17 -o example example.cpp\n",
    "//execute: ./example\n",
    "#include <iostream>\n",
    "\n",
    "int main() {\n",
    "    char a_lower = 'a';\n",
    "    char a_upper = 'A';\n",
    "    \n",
    "    std::cout << a_lower << \" \" << (int) a_lower << \"\\n\";\n",
    "    std::cout << a_upper << \" \" << (int) a_upper << \"\\n\";\n",
    "    a_lower += 5;\n",
    "    std::cout << a_lower << \" \" << (int) a_lower <<\"\\n\";\n",
    "\n",
    "    int c = 97;\n",
    "    int f = c * 1.8 + 32;\n",
    "    std::cout << c << \"  Celsius to \" << f << \" Fahrenheit\" << \", \" << c * 1.8 + 32 << \"\\n\";\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "a 97\n",
    "A 65\n",
    "f 102\n",
    "97  Celsius to 206 Fahrenheit, 206.6\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Floating-Point Types\n",
    "The following table shows the available floating-points and their limits with C++.\n",
    "\n",
    "| Type | Storage Size | Value Range | Precision|\n",
    "|------|--------------|-------------|----------|\n",
    "|float | 4 bytes | 1.2E-38 to 3.4E+38 | 6 decimal places|\n",
    "|double| 8 bytes | 2.3E-308 to 1.7E+308 | 15 decimal places |\n",
    "|long double | 10 bytes |3.4E-4932 to 1.1E+4932 | 19 decimal places|\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: float.cpp\n",
    "//compile: g++ -std=c++17 -o float float.cpp\n",
    "//execute: ./float\n",
    "#include <iostream>\n",
    "#include <limits>\n",
    "\n",
    "int main() {\n",
    "    std::cout << \"double:\\n\" <<\n",
    "                 \"  precision bits: \"   << std::numeric_limits<double>::digits      << \"\\n\" <<\n",
    "                 \"  signed: \" << std::numeric_limits<double>::is_signed   << \"\\n\" <<\n",
    "                 \"  min: \"    << std::numeric_limits<double>::min() << \"\\n\" <<\n",
    "                 \"  max: \"    << std::numeric_limits<double>::max() << \"\\n\"\n",
    "                 \"  exponent bits: \" << \n",
    "                    sizeof(double) * 8 -  \n",
    "                    std::numeric_limits<double>::digits -\n",
    "                    std::numeric_limits<double>::is_signed << \"\\n\";\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should modify the program to look at the float and long double types.\n",
    "\n",
    "In this class, we will primarily use `double`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Overflow and Underflow\n",
    "\n",
    "One of the dangers with C++ and the type representation is that it's possible for the value of an int variable to overflow - this condition occurs when a calculation produces a result that can longer be represented within the given type (its magnitude is too great.).  Similarly, an underflow occurs when the magnitude becomes smaller than the smallest value representable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: overflow.cpp\n",
    "//compile: g++ -std=c++17 -o overflow overflow.cpp\n",
    "//execute: ./overflow\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "int main() {\n",
    "    int value = 3;\n",
    "\n",
    "    for (int i = 1; i < 21; i++)  {\n",
    "        cout << i << \" \" << value << \"\\n\";\n",
    "        value *= 3;\n",
    "    }\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Loss of Precision\n",
    "\n",
    "Loss of precision issues occur with floating-point numbers when the true result of a floating point operation has a smaller precision than can represented in the given data type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: precision.cpp\n",
    "//compile: g++ -std=c++17 -o precision precision.cpp\n",
    "//execute: ./precision\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "int main() {\n",
    "    float value = 0.3;\n",
    "\n",
    "    for (int i = 1; i < 12; i++)  {\n",
    "        cout << i << \" \" << value << \"\\n\";\n",
    "        value /= 300000;\n",
    "    }\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Type Conversions\n",
    "The C++ compiler will implicitly convert data types when a lower data type can be converted into a higher one without losing meaning. \n",
    "\n",
    "![Conversion Hierarchy](images/conversionHierarchy.jpg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: conversion.cpp\n",
    "//compile: g++ -std=c++17 -o conversion conversion.cpp\n",
    "//execute: ./conversion\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "int main() {\n",
    "    char c = 'A';\n",
    "    short s = 1;\n",
    "    int i = 1000;\n",
    "    float f = 1.05;\n",
    "    s = s + c;\n",
    "    i = i + s;\n",
    "    f = f + i + c;\n",
    "    cout << \"s: \" << s << \"\\n\";\n",
    "    cout << \"i: \" << i << \"\\n\";\n",
    "    cout << \"f: \" << f << \"\\n\";\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "s: 66\n",
    "i: 1066\n",
    "f: 1132.05\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Programmers can explicitly perform type conversions in C++ by using type casting. This has the form of `(dataType)` expression where dataType is a valid C++ data type.  Performing such casts may result in the loss of information. C++ will also automatically perform a conversion when values are assigned to a variable or parameter when the destination type does not match.  You can use the g++ option `-Wconversion` to find warnings of such behavior."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: casting.cpp\n",
    "//compile: g++ -std=c++17 -o casting casting.cpp\n",
    "//execute: ./casting\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "int main() {\n",
    "    int sum = 22;\n",
    "    int count = 7;\n",
    "    double mean;\n",
    "\n",
    "    /* implicit conversion after perform integer division */\n",
    "    mean =  sum / count;\n",
    "    cout << \"Value of mean: \" << mean << \"\\n\";\n",
    "\n",
    "    /* explicit conversion: convert an int to a double, then perform float division */\n",
    "    mean = (double) sum / count;\n",
    "    cout << \"Value of mean: \" << mean << \"\\n\";\n",
    "\n",
    "    /* implicit conversion, can be raised as a warning through the gcc option -Wconversion */\n",
    "    int average = mean;\n",
    "    cout << \"Average: \" << average << \"\\n\";\n",
    "\n",
    "    /* programmer demonstrating clear intent to perform the conversion (preferred) */\n",
    "    average = (int) mean;\n",
    "    cout << \"Average: \" << average << \"\\n\";\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Choosing Numeric Data Types\n",
    " Consider the following items to select the most appropriate data  type for a variable:\n",
    "1. Whether the variable needs to hold an integer or floating point numbers\n",
    "2. The maximum and smallest numbers (range) that the variable needs to store.\n",
    "3. For integers, whether the variable needs to hold signed values versus just unsigned values. Typically, if you are counting items (e.g., list size), an unsigned data type should be used.\n",
    "4. For floating point numbers, the number of decimal places (precision) required."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Enumerated Types\n",
    "In C++, an enumeration type (`enum`) is a data type consisting of integral constants. By default, these values start at 0 and increment by 1. We can also explicitly define values. Also, notice in the code that we have defined constants that can used. While C allows programmers to perform arithmetic operations on enums, C++ prevents such behavior - programmers will need to convert values explicitly and ensure the conversion is appropriate.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: enum.cpp\n",
    "//complile: g++ -std=c++17 -o enum enum.cpp\n",
    "//execute: ./enum\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "enum week {Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday};\n",
    "\n",
    "/* change default values */\n",
    "enum minGrade {\n",
    "   F = 0,\n",
    "   D = 60,\n",
    "   C = 70,\n",
    "   B = 80,\n",
    "   A = 90\n",
    "};\n",
    "\n",
    "int main(int argc, char** argv) {\n",
    "    enum week x = Tuesday;   /* declare option to be an enum */\n",
    "    cout << x << \"\\n\";\n",
    "\n",
    "    enum minGrade y = C;\n",
    "    cout << y << \"\\n\";\n",
    "\n",
    "    enum minGrade a = (enum minGrade) 90;\n",
    "    cout << a << \"\\n\";\n",
    "\n",
    "    // Be careful - C++ does not validate the numbers when casting\n",
    "    enum minGrade e = (enum minGrade) 95;\n",
    "    cout << e << \"\\n\";\n",
    "    return 0;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "C++ can also define enum classes in addition to the \"plain\" enumerations presented here.\n",
    "## void\n",
    "The `void` type specifies that no value is available - think of it as \"not present\". We cannot create variables of the type `void`. C++ uses `void` for several reasons:\n",
    "1. Functions with no return value.&nbsp; Such functions are declared as <code class=\"inline-code\">void functionName(int argument) { }</code>\n",
    "2. Functions with no arguments.&nbsp; For example, <code class=\"inline-code\">int functionName(void) {}</code>.&nbsp; &nbsp;Unlike C, C++ allows us to leave the keyword <code class=\"inline-code\">void</code> off in this situation.\n",
    "3. Pointers to void.&nbsp; Pointers of type <code class=\"inline-code\">void *</code> represent the address of an object, but not its type.&nbsp; We'll need to convert (cast) these to an appropriate type for use.&nbsp; We will present pointers in a later notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Strings\n",
    "C++ has two primary ways of representing strings. (Additional representations are available for Unicode-based strings which will not be covered in this class.)\n",
    "As C++ was derived from C and attempts to maintain backward compatibility, the C representation is one way. Here, strings are represented as one-dimensional arrays of characters terminated by a null character `'\\0'`. This method can be error-prone and can lead to security issues, such as buffer overflows if these strings are not handled carefully.\n",
    "The preferred way is to use the `std::string` class provided by the C++ Standard Library encapsulates and simplifies the manipulation of strings.\n",
    "```c++\n",
    "#include <string>\n",
    "\n",
    "std::string myString = \"Hello, World!\";\n",
    "```\n",
    "With std::string, the class -\n",
    "- represents strings as a dynamic array of characters tracking the actual length of the string.\n",
    "- provides many built-in functions for string manipulation, like <code>append</code>, <code>insert</code>, <code>replace</code>, <code>substr</code>, etc.\n",
    "- handles memory allocation and deallocation automatically, reducing the risk of memory leaks and errors.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Class and Structures\n",
    "Classes allow us to combine a group of related variables and keep them together as a single unit. As with Python, we'll also be able to define behavior (methods associated with this data).\n",
    "As C++ uses C as its original basis, we'll first examine how structs were defined in C. As you can see from the code below, structs in C only contain data elements. Most programmers as used `typedef` to be able to use a more \"user-friendly\" type name such as `point`. In C, we cannot just use `_point` as the type name - we'd have to use `struct _point` - C++, though, allows us to use `_point` as the type name.\n",
    "We refer to a member of a particular structure by using `.` (structure member operator) - see lines 19 and 20. We can also provide a list of initializer values to a structure, as presented in line 16. In C,\n",
    "structures may not be compared with the comparison operators such as `==`. However in C++, a programmer can  override those operators in a struct definition and provide the necessary functionality.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "c"
    }
   },
   "outputs": [],
   "source": [
    "//filename: struct.c\n",
    "//complile: gcc -o struct struct.c\n",
    "//execute: ./struct\n",
    "#include <stdio.h>\n",
    "#include <stdlib.h>\n",
    "\n",
    "typedef struct _point point;\n",
    "struct _point {\n",
    "    int x;\n",
    "    int y;\n",
    "};\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    point p;\n",
    "    \n",
    "    p.x = 1;\n",
    "    p.y = 5;\n",
    "\n",
    "    point q = {40, 60};\n",
    "\n",
    "    printf(\"(x,y): (%d,%d)\\n\", p.x, p.y);\n",
    "    printf(\"(x,y): (%d,%d)\\n\", q.x, q.y);\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "c"
    }
   },
   "outputs": [],
   "source": [
    "//filename: struct_examples.c\n",
    "//complile: gcc -o struct_examples struct_examples.c\n",
    "//execute: ./struct_examples\n",
    "#include <stdio.h>\n",
    "#include <stdlib.h>\n",
    "\n",
    "typedef struct _point point;\n",
    "struct _point {\n",
    "    int x;\n",
    "    int y;\n",
    "};\n",
    "\n",
    "typedef struct _rect rect;\n",
    "struct _rect {\n",
    "    point pt1;\n",
    "    point pt2;\n",
    "};\n",
    "\n",
    "typedef struct _circle circle;\n",
    "struct _circle {\n",
    "    point center;\n",
    "    float radius;\n",
    "};\n",
    "\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    circle c = { {2,1}, 5};\n",
    "    printf(\"(%d,%d) - area: %f\\n\", c.center.x, c.center.y, c.radius * c.radius * 3.14);\n",
    "\n",
    "    point a = {6,6};\n",
    "    point b = {3, -6};\n",
    "    rect r;\n",
    "    r.pt1 = a;\n",
    "    r.pt2 = b;\n",
    "    printf(\"(%d,%d) (%d,%d), area: %d\\n\", r.pt1.x, r.pt1.y, r.pt2.x, r.pt2.y, abs(r.pt1.x - r.pt2.x) * abs(r.pt1.y - r.pt2.y)   );\n",
    "\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "C++ builds upon this foundation by allowing us to define behavior within a `struct`. The language also adds the keyword `class`. `struct` and `class` provide the same capabilities to programmers. They differ, though, in the default visibility of their members. In a `struct` type, members have public visibility by default - that is, programmers can directly access those members (variables and functions). In a `class` type, members have private visibility by default, requiring methods with public visibility to be able to indirectly reference them. In both `class` and `struct`, it is possible to create sections of different accessible modifiers (`private`, `public`, and `protected`). We will primarily use classes, explicitly setting the visibility modifier to public. The following code block demonstrates this -\n",
    "\n",
    "```c++\n",
    "class RetireInfo {\n",
    "    public:\n",
    "    int months;\n",
    "    double contribution;\n",
    "    double rate_of_return;\n",
    "};\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: class.cpp\n",
    "//complile: g++ -std=c++17 -o class class.cpp\n",
    "//execute: ./class\n",
    "#include <iostream>\n",
    "\n",
    "class Point {\n",
    "    public:\n",
    "    int x;\n",
    "    int y;\n",
    "};\n",
    "\n",
    "class Rect {\n",
    "    public:\n",
    "    Point pt1;\n",
    "    Point pt2;\n",
    "    double area() {\n",
    "        return abs(pt1.x - pt2.x) * abs(pt1.y - pt2.y);\n",
    "    }\n",
    "};\n",
    "\n",
    "struct Circle {\n",
    "    Point center;\n",
    "    double radius;\n",
    "\n",
    "    double area() {\n",
    "        return radius * radius * 3.14;\n",
    "    }\n",
    "};\n",
    "\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    Circle c = { {2,1}, 5};\n",
    "    std::cout << \"(\" << c.center.x << \", \" << c.center.y << \") - area: \" << c.area() << \"\\n\";\n",
    "\n",
    "    Point a = {6,6};\n",
    "    Point b = {3, -6};\n",
    "    Rect r;\n",
    "    r.pt1 = a;\n",
    "    r.pt2 = b;\n",
    "    std::cout << \"(\" << r.pt1.x << \", \" << r.pt1.y << \") (\" << r.pt2.x << \", \" << r.pt2.y << \") - area: \" << r.area() << \"\\n\";\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note: We intentionally used the `struct` keyword with Circle to demonstrate the difference between `struct` and `class`. We also added some behavior to start introducing the concept of object-oriented programming that you have already seen with Python. For this class, we will predominantly use `class` rather than `struct`. If nothing else, `class` makes the programmer explicitly choose which attributes and methods to be public."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
