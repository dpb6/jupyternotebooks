#include <iostream>
using namespace std;

enum week {Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday};

/* change default values */
enum minGrade {
   F = 0,
   D = 60,
   C = 70,
   B = 80,
   A = 90
};

int main(int argc, char** argv) {
    enum week x = Tuesday;   /* declare option to be an enum */
    cout << x << "\n";

    enum minGrade y = C;
    cout << y << "\n";

    enum minGrade a = (enum minGrade) 90;
    cout << a << "\n";

    // Be careful - C++ does not validate the numbers when casting
    enum minGrade e = (enum minGrade) 95;
    cout << e << "\n";
    return 0;
}