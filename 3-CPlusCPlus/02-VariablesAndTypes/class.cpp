#include <iostream>

class Point {
    public:
    int x;
    int y;
};

class Rect {
    public:
    Point pt1;
    Point pt2;
    double area() {
        return abs(pt1.x - pt2.x) * abs(pt1.y - pt2.y);
    }
};

struct Circle {
    Point center;
    double radius;

    double area() {
        return radius * radius * 3.14;
    }
};


int main(int argc, char *argv[]) {
    Circle c = { {2,1}, 5};
    std::cout << "(" << c.center.x << ", " << c.center.y << ") - area: " << c.area() << "\n";

    Point a = {6,6};
    Point b = {3, -6};
    Rect r;
    r.pt1 = a;
    r.pt2 = b;
    std::cout << "(" << r.pt1.x << ", " << r.pt1.y << ") (" << r.pt2.x << ", " << r.pt2.y << ") - area: " << r.area() << "\n";

    return EXIT_SUCCESS;
}