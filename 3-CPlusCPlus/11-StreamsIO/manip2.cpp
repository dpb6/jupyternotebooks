#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main(int argc, char *argv[]) {
	// Formatting floating Point Numbers
	double num = 123;
	double val = 431.12234;
	double val2 = 1.237829183772;
	double val3 = 1234567.123;
	
	// Default output
	// Notice the default precision
	// is 6 for double/float that is
	// 6 digits will be displayed
	cout << "Default: " << endl;
	cout << num  << endl
	     << val  << endl
	     << val2 << endl
	     << val3 << endl;
	     
	// setprecision(4) and do some spacing
	cout << endl << "setprecision(4): " << endl;
	cout << setprecision(4); // applies to all future outputs
                             // or until reset
	cout << num  << endl
	     << val  << endl
	     << val2 << endl
	     << val3 << endl;
	
	// setprecision(2) and do some spacing
	cout << setprecision(2) << endl << "setprecision(2): " << endl;
	
	cout << num  << endl
	 << val  << endl
	 << val2 << endl
	 << val3 << endl;
	 
	// add fixed to the mix
	cout << fixed << endl << "setprecision(2) + fixed: " << endl;
	cout << num  << endl
	     << val  << endl
	     << val2 << endl
	     << val3 << endl;

	// add showpoint to the mix
	cout << showpoint << endl << "setprecision(2) + fixed + showpoint: " << endl;
	cout << num  << endl
	     << val  << endl
	     << val2 << endl
	     << val3 << endl;
    return 0;

    return EXIT_SUCCESS;
}