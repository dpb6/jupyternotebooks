#include <sstream>
#include <string>
#include <iostream>

int main(int argc, char *argv[]) {
    std::stringstream ss;
    double d = 3.14;
    int r = 2;

    ss << "Circle: (radius=" << r << ") has area: " << d * r * r;
    
    std::string output = ss.str();
    std::cout << output << std::endl;
}