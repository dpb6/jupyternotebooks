#include <iostream>
#include <fstream>
#include <string>

std::ifstream openFile(std::string name) { // abstract into a function
    std::ifstream in(name);
    if (!in.is_open()) {
        std::cerr << "Unable to open " << name << std::endl;
        exit(EXIT_FAILURE);  // since we are in function, return would go back to main
    }
    return in;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " inputFile ..." << std::endl;
        return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; i++) {
        std::ifstream in = openFile(argv[1]);
        if (!in.is_open()) {
            std::cerr << "Unable to open - " << argv[1] << "\n";
            return EXIT_FAILURE;
        }

        std::string line;
        while (getline(in,line)) {        //continually reads until end of file
            std::cout << line << "\n";    // line does not include a newline
        }

        in.close();   // allocate a resource, use it, release it.
    }
}