#include <iostream>
#include <string>
using namespace std;

int main(int argc, char *argv[]) {
    bool value = true;
    
    std::cout << "boolean: " << value << "\n";
    std::cout << "boolean (after manipulator): " << boolalpha << value << "\n";

    value = false; 
    std::cout << "boolean (manipulator persists): " << value << "\n";
    std::cout << "boolean (reset noboolalpha): " << noboolalpha << value << "\n";

    return EXIT_SUCCESS;
}