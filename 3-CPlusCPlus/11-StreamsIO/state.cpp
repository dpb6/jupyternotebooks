#include <iostream>
#include <iomanip>

int main() {
    // Save the stream state
    std::ios oldState(nullptr);
    oldState.copyfmt(std::cout);
 
    std::cout << std::hex << std::setw(8)  << std::setfill('0') << 0xDECEA5ED << std::endl;

    // restore the stream state
    std::cout.copyfmt(oldState);

    std::cout << std::setw(15)  << std::left << "case closed" << std::endl;

    // .flags() is insufficient
    std::ios_base::fmtflags f( std::cout.flags() ); // save flags in f
    std::cout << std::hex << std::setw(8)  << std::setfill('0') << 0xDECEA5ED << std::endl;
    std::cout.flags( f ); // restore flags
    // setfill is still active
    std::cout << std::setw(15)  << std::left << "case closed" << std::endl;
}

