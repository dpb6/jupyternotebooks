#include <iostream>

int main() {
    int num = -1;
    
    std::cout << "Enter a bad integer: ";

    std::cin >> num;

    if (std::cin.eof()) {
        std::cout << "End of file received\n";
    }
    else if (std::cin.bad()) {
         std::cerr << "Failed to read an integer - bad" << std::endl;
         std::cin.clear(); // Clear the error flags
        // Handle the error or recover as needed.
    }
    else if (std::cin.fail()) {
        std::cerr << "Failed to read an integer." << std::endl;
        std::cin.clear(); // Clear the error flags
        // Handle the error or recover as needed.
    }
    
    return EXIT_SUCCESS;
}