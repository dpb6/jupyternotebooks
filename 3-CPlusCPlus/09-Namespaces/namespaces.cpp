#include <iostream>

namespace MySpace {
    int myVar = 10;
    int alterMyVar() {
        return 2 * myVar;
    }
}

namespace AnotherSpace {
    int myVar = 50;
    int alterMyVar() {
        return 3 * myVar;
    }
}

int main(int argc, char *argv[]) {
    int myVar = 3;

    std::cout << "main: myVar - " << myVar << "\n";
    std::cout << "MySpace: myVar - " << MySpace::myVar << "\n";
    std::cout << "MySpace: calling alterMyVar - " << MySpace::alterMyVar() << "\n";

    {
        using namespace AnotherSpace;  // this using is limited to the scope of {   }
        std::cout << "calling alterMyVar in AnotherSpace - " << alterMyVar() << "\n";
        std::cout << "calling alterMyVar in MySpace - " << MySpace::alterMyVar() << "\n";
    }

    using namespace MySpace;
    std::cout << "calling alterMyVar - " << alterMyVar() << "\n";

    // Note that with the same identifier, C++ uses the current scope declaration.
    std::cout << "myVar - " << myVar << "\n";

    std::cout << "calling alterMyVar in AnotherSpace - " << AnotherSpace::alterMyVar() << "\n";

    return EXIT_SUCCESS;
}