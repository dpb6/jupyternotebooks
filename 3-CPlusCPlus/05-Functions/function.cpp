#include <iostream>
using std::cout;

int max(int num1, int num2) {
   if (num1 > num2) {
       return num1;
   }
   else {
       return num2;
   }
}

int main(int argc, char *argv[]) {
    int num1 = 122, num2 = 22;
    int result = max(num1,num2);
    cout << "Maximum of (" << num1 << ", " << num2 << "): " << result << "\n";
}