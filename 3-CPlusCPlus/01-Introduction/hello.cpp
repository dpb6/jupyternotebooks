#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
    cout << "Hello World\n";

    int countdown = 5;
    for (int i = countdown; i >= 0; i--) {
        cout << i << "\n";
        if (i == 0) {
            cout << "Let's go!" << endl;
        }
    }
    return EXIT_SUCCESS;
}