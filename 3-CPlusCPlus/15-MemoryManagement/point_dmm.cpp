#include <iostream>
using namespace std;

class Point {
private:
    double x = 0, y = 0;
public:
    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}
    double getX() const { return x; }
    double getY() const { return y; }
    void setX(double val) { x = val; }
    void setY(double val) { y = val; }
};

int main(int argc, char *argv[]) {
    size_t numPoints = 5;
    Point *points = new Point[numPoints];
    
    for (size_t i; i < numPoints; i++) {
        cout << i << ": " << points[i].getX() << "," << points[i].getY() << endl;
    }

    delete[] points;
    points = nullptr;   // indicates that we can no longer access the array

    return EXIT_SUCCESS;
}