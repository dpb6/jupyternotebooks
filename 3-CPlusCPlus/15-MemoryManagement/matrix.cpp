#include <iostream>
#include <iomanip>
using namespace std;

class Point {
private:
    double x = 0.0, y = 0.0;
public:
    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}
    double getX() const { return x; }
    double getY() const { return y; }
    void setX(double val) { x = val; }
    void setY(double val) { y = val; }
};

int main(int argc, char *argv[]) {
    int rows = 3, columns = 5; 
    
    Point **matrix = new Point*[rows];  // allocate space for the row pointers
 
    for (int i = 0; i < rows; i++) {       // allocate space for the columns in each row
        matrix[i] = new Point[columns];
    }
 
    //Saves the current state of cout flags (e.g., precision)
    std::ios cout_state(nullptr);
    cout_state.copyfmt(std::cout);

    cout << setprecision(2);  // precision remains set until changed
    cout << fixed;
    cout << "[";
    for (int i = 0; i < rows; i++) {
        if (i > 0) { cout << endl << " "; }
        for (int j = 0; j < columns; j++) {
            cout << "(" << setw(6) << matrix[i][j].getX() << ","  
                        << setw(6) << matrix[i][j].getY() << ")";
        }
    }
    cout << "]" << endl;
    
    // restore the state of cout
    std::cout.copyfmt(cout_state);
    
    // free the allocated memory - just reverse the stesp
    for (int i = 0; i < rows; i++) { // delete inner arrays (data for each row)
        delete[] matrix[i];
    }
    delete[] matrix;
    return 0;
}