# Core Python / Files

1. *What is the purpose of the `open()` function in Python, and what are the different modes you can use with that function? What is the default mode?*

   The `open()` function is used to open a file. The function provides a way to interact with 
   files on the file system, allowing you to read from or write to them.

   The open() function takes two arguments:
   - *file_path*: This is a string representing the path to the file you want to open. It can be a relative path 
     (e.g., 'data.txt') or an absolute path (e.g., '/path/to/data.txt').
   - mode (optional): This is a string that specifies the mode in which the file should be opened. 
     If not provided, the default mode is 'rt' (read text file).

   The different modes you can use with the open() function are:
   - `'r'` (read mode): This mode is used to read the contents of a file. It is the default mode.
   - `'w'` (write mode): This mode is used to write to a file. If the file already exists, 
      its contents will be truncated (i.e., removed). If the file does not exist, a new file will be created.
   - `'a'` (append mode): This mode is used to append data to the end of an existing file. If the file does not exist, a new file will be created.
   - `'x'` (exclusive creation mode): This mode is used to create a new file but raises a FileExistsError if the file already exists.
   - `'b'` (binary mode): This mode is used for reading or writing binary data (such as images or executable files). 
      It can be combined with other modes (e.g., 'rb' for reading binary data, 'wb' for writing binary data).
   - `'t'` (text mode): This mode is used for reading or writing text data (such as plain text files or 
      Python source code files). It is the default mode and can be omitted.
   
   You can also combine read and write:
   - `'r+’` (read and write mode): This mode is used to read from and write to a file. It doesn’t truncate the file, preserving its existing contents.
   - `'w+'` (write and read mode): This mode is similar to ‘r+’, but truncates the file, removing existing contents.

2. *What is the difference between the read(), readline(), and readlines() methods when reading a file?*
   - `read()` reads the entire file as a single string.
   - `readline()` reads one line at a time, retaining the newline character.
   - `readlines()` reads all lines and returns them as a list of strings, where each string 
     represents a line from the file, including the newline character.

3. *Explain how to use the file object as an iterator to read data from a text file.*

    You can use the file object as an iterator to read data from a text file line by line. 
    This can be particularly useful when dealing with large files, as it allows you to process 
    the data in smaller chunks rather than loading the entire file into memory at once.

    1. Open the file: First, open the file using the open() function in read mode ('r').
   
       ```python
       codefile = open('file.txt', 'r')
       ```
    2. Iterate over the file object: Once the file is open, you can iterate over the file object directly 
       using a `for` loop. Each iteration will yield the next line of the file (including the newline character `\n`).
 
       ```pthon
       for line in codefile:
           print(line)
       ```
       
    3. Close the file: After you've finished reading from the file, it's important to close the file to free up system resources.
       ```python
       codefile.close()
       ```

4. *Which reading method(s) should be used to read large files?*

   You should either use `readline()` or iterate through the file. Using `read()` or `readlines()` could lead to 
   memory issues with extremely large files.

5. *How do you write data to a file in Python? Explain the steps.*
 
   You first need to open the file for writing, then output the data to the file, and, finally, close the file when you
   are complete.  You can output data by using the `write()` method on the file object or using `print()` and specifying
   the file object parameter. See question 9 for more details.

6. *What is the purpose of the `with` statement when working with files, and how does it help with resource management?*

   The with statement in Python is used in conjunction with context managers to ensure proper 
   resource acquisition and release. When working with files, the with statement is particularly 
   useful for automatically closing the file after operations are completed, even in the presence 
   of exceptions or errors.

   Here's how the with statement helps with resource management when working with files:
   - Automatic acquisition of resources: The `with` statement sets up the context manager by 
     acquiring the necessary resources (in this case, opening the file).
   - Guaranteed release of resources: When the code inside the `with` block finishes executing, 
     or if an exception occurs, the context manager automatically releases the acquired resources 
     (closing the file). This ensures that the file is properly closed, regardless of the execution path.
   - Exception handling: If an exception occurs within the `with` block, the context manager 
     still properly releases the resources before propagating the exception.

   Without using the with statement, you would need to manually open and close the file, and 
   ensure that the `close()` method is called in both the normal execution path and the 
   exception handling path. This can lead to potential resource leaks if the `close()` method is not called properly.

   The following function echos a file's contents to the terminal
   ```python
   def echo(filename):
       with open(filename, 'r') as f:
           for line in f:
               print(line.rstrip())
   ```

7. *Should resources be left open to optimize execution speed? Discuss possible tradeoffs.*

   Leaving resources open to optimize execution speed is generally not recommended, as it 
   can lead to resource leaks, instability, and potential security vulnerabilities. 
   However, in certain scenarios, it may be a valid optimization technique, but it should be 
   carefully evaluated and implemented with proper resource management practices.

   A common place in which resources are left open is database connections. Existing libraries,
   though, provide monitoring capabilities as well as periodic resource cleanup.

8. *How do you open a file in append mode, and what does it allow you to do?*
 
   You can open a file in append mode using the `'a'` mode with the `open()` function. 
   The append mode allows you to write new data to the end of an existing file without 
   overwriting its current contents.

9. *Compare and contrast the `print()` and `write()` functions.*
  
   The print() and write() functions can both output data, but they serve different purposes and have distinct characteristics, especially when it comes to writing data to a file.

   `print()` Function: Primarily used for displaying output to the console, but can direct output to a file.
   ```python
   print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)
   ```
   Key Features of `print()`:
   - Multiple Objects: Can take multiple objects and print them with a specified separator (sep).
   - End Character: By default, adds a newline character (\n) at the end of the output. This can be changed using the end parameter.
   - Redirection: Output can be redirected to a file by specifying the file parameter.
   - Formatting: Automatically converts non-string objects to strings.

   ```python
   with open('output.txt', 'w') as f:
       print('Hello, World!', file=f)
       print('Another line', file=f)
   ```

   `write()` Method: Specifically designed for writing raw data to a file. Available on 
   file objects returned by open() in write or append mode.
   ```python
   file.write(string)
   ```
   Key Features:
   - Single String Argument: Takes exactly one string argument. No automatic conversion of non-string types.
   - No Extra Characters: Does not add any newline or other characters unless explicitly included in the string.

10. *How do you read and write binary data to a file in Python?*

    You can read and write binary data to a file by using the 'b' (binary) mode in conjunction 
    with the open() function and the appropriate methods for reading and writing binary data.

    *Writing Binary Data to a File*

    To write binary data to a file, you need to open the file in binary write mode (`'wb'`) 
    and use the `write()` method with bytes-like objects (bytes or bytearrays).
    ```python
    # Open the file in binary write mode
    with open('output.bin', 'wb') as file:
        # Write binary data to the file
        data = b'\x01\x02\x03\x04'
        file.write(data)

        # Write more binary data
        more_data = bytearray([5, 6, 7, 8])
        file.write(more_data)
    ```

    *Reading Binary Data from a File*

    To read binary data from a file, you need to open the file in binary read mode (`'rb'`) 
    and use the `read()` method to read the binary data into bytes-like objects.
    ```python
    # Open the file in binary read mode
    with open('output.bin', 'rb') as file:
        # Read binary data from the file
        binary_data = file.read()
        print(binary_data)  # Output: b'\x01\x02\x03\x04\x05\x06\x07\x08'
    ```

    You can also read binary data in smaller chunks by specifying the number of bytes to read:
    ```python 
    with open('output.bin', 'rb') as file:
        # Read 4 bytes of binary data
        chunk = file.read(4)
        print(chunk)  # Output: b'\x01\x02\x03\x04'
    ```

    When working with binary data, it's important to open the file in binary mode (`'rb'` or `'wb'`), 
    as the default text mode ('r' or 'w') may introduce unwanted behavior, such as newline 
    translations or encoding/decoding issues.