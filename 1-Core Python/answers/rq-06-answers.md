# Core Python / Control Statements

1. *What is the type and value of x?*

   int 10

   Use type(x) and print(x) to verify. `or` returns the actual value of the first "true" expression.  For `and` if the clause evaluates to `True`, the last expression evaluated is returned.

2. *Why is the order of `if` and `elif` clauses important?*

   The Python interpreter evaluates the clauses in their listed order. Only the code block associated with the first clause that evaluates to true executes.

3. *Explain short-circuit evaluation for `and` and `or` clauses.*

   Once the Python interpreter can determine the outcome of a Boolean expression, the interpreter no longer needs to continue evaluation.

   For `and`, if the first expression evaluates to `False`, the second expression will not be evaluated as the result of the operator will always be `False`.

   For `or`, if the first expression evaluates to `True`, the second expression will not be evaluated as the result of the operator will always be `True`.
   
4. *How are code blocks identified in Python?*

   Indention.  This indention must be consistent throughout the source code file. Other programming languages often use curly braces.

5. *What is the difference between `if/elif` statement(s) and a series of `if` statements?*
   
   With `if/elif` statements, only the code block associated with the first clause that evaluates to `True` executes. With a series
   of `if` statements, each code block associated with a `True` clause will evaluate.

6. *Why is testing for equality with float numbers problematic?  How do we work around such situations?*

   Floating-point numbers do not have an exact representation. As such certain calculations may not produce the exact answer expected. 
   Use the [`math.isclose()`](https://docs.python.org/3/library/math.html#math.isclose) instead of the equality operator.
   