# Core Python / Testing

1. *What is the goal of testing?*

   To increase confidence that programs work correctly and that those programs meet our customer's expectations. 
   It is impossible to verify the absence of all defects in a program.

2. *Summarize the primary structure of test cases*

   The structure consists of defining the inputs and then the expected outputs. We will also need to perform preparation for the test (setting up the environment/program state), execute the test, validate the expected results, and then perform any necessary clean-up activities.

3. *What are assertions? How do we utilize assertions in test cases? What does an assert statement do?*

   Assertions are statements of fact. We use assert statements to validate that the program state is valid. In other words, does a computation match
   the expected value. If the condition fails in the assert statement, an exception is raised.

4. *How do equivalence partitions and boundary value analysis help us create effective test cases?*

   We use these approaches to identify representative values (equivalence) to minimize the number of cases and values (boundaries) where issues often occur.  
   
5. *How do reviews lead to high-quality software?*
   
   By having another developer examine the source code directly, that developer may be able to find issues missed by the original developer or suggest alternative improvements. 
   Additionally, reviewers become familiar with the changes, increasing their knowledge of the system. 
   Reviewers can also act as mentors to the original programmer.

6. *Why is coverage important in testing? What weaknesses does coverage have?*

   Coverage is important in testing because it provides a quantitative measure of how thoroughly 
   the software has been tested, identifying areas that have been exercised by the test suite and 
   areas that remain untested. However, coverage has weaknesses: achieving high coverage does not 
   guarantee the absence of defects, as it does not consider the quality or effectiveness of the 
   test cases themselves. Additionally, certain types of defects, such as those related to specific 
   input combinations or timing issues, may not be effectively covered by traditional coverage metrics. 
   Therefore, coverage should be used as a guide rather than an absolute indicator of test quality.

7. *How does the number of paths through a control flow graph grow?*

   Exponentially with the number of conditionals.

8. *Why is it important for tests to be repeatable and automated?*

   If a test is not repeatable, no guarantee exists as to whether or not the correct results occur - verification is impossible.
   With automation, we can execute test cases whenever changes occur.

9. *Assume, you have a function that takes a list of numbers and returns the sum of all even numbers in the list. 
   What test cases should be created for this function?*
   
   - Empty list
   - List with one item that is even
   - List with one item that is odd
   - List with multiple items (general case)
   - List with two items (smallest interesting case for sorting)

10. *Assume you have a function takes a temperature in Celsius and returns the corresponding temperature in Fahrenheit.
    What test cases should be created for this function?*

    - Temperature within a typical range (e.g., 20°C)
    - Temperature at or near the freezing point of water (0°C)
    - Temperature at or near the boiling point of water (100°C)
    - Extremely low temperature (e.g., -50°C)
    - Extremely high temperature (e.g., 150°C)

11. *You have a function that checks the number of assets in an investment portfolio. The maximum number of assets allowed 
    is twenty. What test cases should be created for this function?*

    - Test with no assets.
    - Test with one asset.
    - Test with the exact maximum allowed number of assets.
    - Test with more than the maximum allowed number of assets.

12. *You have a function that takes an applicant's credit score and determines if they are eligible for a loan (credit score >= 650).
    The maximum credit score is 850. What test cases should be created for this function?*

    - Credit score below the threshold (e.g., 600, 649)
    - Credit score at the lower boundary (650)
    - Credit score above the lower boundary (e.g., 700, 800)
    - Credit score at the upper boundary (e.g., 850, if applicable)
    - Credit score above the upper boundary (e.g., 900, if applicable)

13. *You have a function that takes a credit card number as input and returns True if it is a valid number, False otherwise.
    What test cases should be created for this function?*
  
    - Valid credit card number
    - Invalid credit card number (e.g., wrong length, invalid characters)
    - Empty string

14. *You have a function that takes an amount in one currency and converts it to another currency 
    based on the current exchange rate (retrieved from an external source).
    What test cases should be created for this function?*

    - Conversion with valid currencies and exchange rate
    - Conversion with an invalid currency
    - Conversion with unsupported currency
    - Conversion when the external exchange rate source is unavailable