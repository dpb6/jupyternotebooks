# Core Python / Basic Input and Output

1. *Why is input and output so fundamental to computer programs?*

   I/O enables interaction with the environment around a program/system.  Without it, we know less
   than an observer of [Schrödinger's Cat](https://en.wikipedia.org/wiki/Schr%C3%B6dinger%27s_cat).

2. *What is the data type returned by `input()`? How can we convert that value to other types?*

   `input()` returns a str (string) datatype. For built-in types such as `int` and `float`,
   we can use the built-in functions `int()` and `float()` to convert string values to those tpyes.

3. *Why is readability important?*
   
   Readability is crucial to understanding a program. Not only do we need this as we build the
   program, but also as we have to maintain (fix defects, add functionality) the program in the future. 

4. *How are comments identified in Python?*

   Anything from `#` to the end of the line.
   
5. *How many arguments does `input()` require?*

   Zero. The function takes an optional argument that specifies the prompt to the user.
   The prompt tells the user the expected input. For a well-defined or simplistic interface, that may not be required.

6. *How many arguments can `print()` have?*

   Undetermined. It can have zero, but also many. Optional named arguments can also specify the characters printed
   between those argument values as well as the character printed at the end of all of them. Execute `help("print")`.
