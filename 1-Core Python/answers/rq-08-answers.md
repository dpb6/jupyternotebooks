# Core Python / Strings

## Review Questions
1. *How are strings represented in Python?*

   Strings are ordered sequences of Unicode characters in Python.

2. *Compare and contrast ASCII versus Unicode representations. Does Python 3 offer a choice of which representation can be used?*


   No. Python 3 only uses Unicode.  (discuss raw and byte? strings)

3. *Can strings be changed once they are created? What advantages and disadvantages does this have?*

   No, strings are immutable. Anytime a change appears to be made to a string, the interpreter creates a new string.

   Immutability ensures that once a string is created, its value cannot be changed. This prevents unintended modifications to strings, leading to more predictable behavior in programs. In multi-threaded programs, immutable strings are inherently thread-safe because they cannot be modified after creation. This eliminates the need for synchronization mechanisms like locks, simplifying concurrency management. Immutability allows Python to optimize memory usage by reusing existing string objects. Since strings cannot change, Python can safely reuse existing strings without creating unnecessary copies. Immutable objects like strings can be used as keys in dictionaries and elements in sets because their values cannot change.

   For disadvantages, When concatenating strings, Python creates new string objects for each intermediate result, leading to increased memory usage.
   Additionally, certain string operations, such as slicing or concatenation, may involve creating new string objects, leading to performance overhead, especially for large strings or frequent operations. Mutable string buffers can perform such operations more efficiently.

4. *How are strings indexed in Python? Provide examples for a single character, starting from the start, middle, and end of a string.*

   Strings are indexed using the square brackets operator `[]`. With that operator, we can extract a single character (which is just 
   a string in Python) or another substring. Positions start at zero.

   ```python
   p = "Financial Technology"
   p[0]    # F
   p[10]   # T
   p[19]   # y
   p[-1]   # y
   p[10:14]  # Tech
   p[10:]    # Technology
   ```

5. *What advantages does negative indexing provide? Provide an example.*

   Negative indexing in Python strings allows you to access characters from the end of the string, counting backward from -1. This can be particularly useful when you want to manipulate or access characters at the end of a string without knowing or calculating the exact length of the string.

   As an example, negative indexing can be particularly useful when working with strings that represent file paths or URLs, where you might want to extract the file extension or the domain name without having to parse the entire string explicitly.

6. *What function returns the length of the string?*

   `len()`

7. *How would you remove leading and trailing whitespace from a string representing a company's address?*

   `strip()`

8. *Given a string representing a company's financial report, how would you replace all occurrences of the word "profit" with "revenue"?*

   `s.replace("profit","revenue")`


## Drill Steps
```python
u = "Duke University"
len(u)
u[0:4]   or u[:4]
u.find("U")
u[5:]  or u[5:15]
u[-10:]
u[3:6]
u[8]
"fintech" in u x = "   finance  "
x.strip()
x.lstrip()
x.replace(" ","X")
u.upper()
u.rfind("e")
u+x
u[::2]
```
