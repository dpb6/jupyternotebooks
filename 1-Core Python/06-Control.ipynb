{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "83a93765",
   "metadata": {},
   "source": [
    "<div class=\"pagebreak\"></div>\n",
    "\n",
    "# Control Statements\n",
    "So far, we have seen some basic programs and uses of data, but we have not had to make any decisions to decide what code to execute or to repeat the execution of certain lines of code. Control statements allow us to alter a program's behavior based on different conditions within the data.\n",
    "\n",
    "In our daily lives, we run across countless situations where we choose different behavior based on some condition:\n",
    "- What to wear based on the weather\n",
    "- When to wake up and go to sleep - based upon the time of day\n",
    "- Converting values into different types - for a given numeric grade, produce the equivalent letter grade\n",
    "- Restrictions on who can drive or drink. (and it better not be both at the same time!)\n",
    "\n",
    "In this notebook, we will see how to use an `if` statement to execute different code blocks based upon a conditional expression that evaluates to a Boolean (`True` or `False`)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "663d7b2e",
   "metadata": {},
   "source": [
    "## if statement\n",
    "We use the `if` statement to execute a code block when a specified condition is `True`.  The basic syntax:\n",
    "<pre>\n",
    "if condition:\n",
    "    <i>block</i>\n",
    "</pre>\n",
    "If the condition evaluates to `True`, then the statement(s) in the block are executed.  \n",
    "\n",
    "Below, we present a [flowchart](https://en.wikipedia.org/wiki/Flowchart) to represent the execution of an `if` statement.  Flowcharts can help you visualize and understand a process. We use flowcharts to show the overview of different Python statements.  Initially, you may want to draw flowcharts to help you understand a problem or a program, but flowcharts are too cumbersome to use in practice.\n",
    "- Ovals or rounded rectangles indicate the beginning or end of a program. *terminal*\n",
    "- Rectangles represent one or more operations that occur in sequence. *process*\n",
    "- Diamonds represent a conditional operation to choose which path a program or process will follow. *decision*\n",
    "- Arrows represent the program's (process's) order of operation.  Labels may indicate decision paths.  *flowline*\n",
    "- Parallelograms (not shown) represent data input and output.\n",
    "\n",
    "You can augment these diagrams with whatever notes, variable assignments, etc., to help document a given process."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0eaeee34",
   "metadata": {},
   "source": [
    "![](images/ifStatement.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "823ec3f6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "What is your age? 23\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You are old enough to drive in most US states.\n"
     ]
    }
   ],
   "source": [
    "age = input(\"What is your age?\")\n",
    "if int(age) >= 16:\n",
    "    print(\"You are old enough to drive in most US states.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1092393d",
   "metadata": {},
   "source": [
    "In the above example, `int(age) >= 16` is the conditional expression.  With Python, this is any expression that evaluates to  `True` or `False`.  Recall that any nonzero numeric value is `True`, and zero is `False`. For strings, any non-empty string is `True`, and an empty string is `False`.  Of course, we can use Boolean variables as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "c1a1d608",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Let's celebrate!\n"
     ]
    }
   ],
   "source": [
    "team_won = True\n",
    "if team_won:\n",
    "    print (\"Let's celebrate!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "afc9bb33",
   "metadata": {},
   "outputs": [],
   "source": [
    "if 0:\n",
    "    print(\"nothing to see here - zero evaluates to false\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "91917643",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hey - I'm here\n"
     ]
    }
   ],
   "source": [
    "if -1:\n",
    "    print(\"Hey - I'm here\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "f90d036b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hey - I was true. non-empty strings are true\n"
     ]
    }
   ],
   "source": [
    "if \"message\":\n",
    "    print(\"Hey - I was true. non-empty strings are true\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "43868dd1",
   "metadata": {},
   "outputs": [],
   "source": [
    "if \"\":\n",
    "    print(\"Sting empty, evaluates to false,  you can't see me!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c480e52",
   "metadata": {},
   "source": [
    "In the above examples, the print statements after the `if` statements form the \"true-block\". These code blocks can be any number of statements, including nested `if` statements."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4775d0a1",
   "metadata": {},
   "source": [
    "## Comparison Operators\n",
    "Sometimes, using a single value is sufficient for an `if` statement. Python, though, supports several conditional operators:\n",
    "\n",
    "| Comparison<br>Operator | Example | Meaning |\n",
    "| :---: | :---- | :---- |\n",
    "| `>` | `x > y` | `x` is greater than `y`\n",
    "| `<` | `x < y` | `x` is less than `y`\n",
    "| `>=` | `x >= y` | `x` is greater than or equal to `y`\n",
    "| `<=` | `x <= y` | `x` is less than or equal to `y`\n",
    "| `==` | `x == y` | `x` is equal to `y`\n",
    "| `!=` | `x != y` | `x` is not equal to `y`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "3f1bd064",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4 < 5: True\n",
      "10 > 5: True\n",
      "5 <= 6: True\n",
      "Hello: True\n"
     ]
    }
   ],
   "source": [
    "print(\"4 < 5:\", 4 < 5)\n",
    "print(\"10 > 5:\", 10 > 5)\n",
    "print(\"5 <= 6:\", 5 <= 6)\n",
    "print(\"Hello:\", \"hello\" == \"hel\"+\"lo\")  # Use string concatenation to create \"hello\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fc8a9c7",
   "metadata": {},
   "source": [
    "## if else statement\n",
    "In addition to executing the true block, Python allows for an optional `else` block that executes if the condition evaluates to `False`.\n",
    "\n",
    "![](images/ifElseStatement.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "1f438e89",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You are not old enough to drive.\n",
      "You will need to wait until you are 16.\n"
     ]
    }
   ],
   "source": [
    "age = 15\n",
    "if age >= 16:\n",
    "    print(\"You can drive!\")\n",
    "    print(\"You need a license first, though...\")\n",
    "else:\n",
    "    print(\"You are not old enough to drive.\")\n",
    "    print(\"You will need to wait until you are 16.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "712c0fe4",
   "metadata": {},
   "source": [
    "In Python, indention determines the code blocks that execute for the `if` statement.  The Python convention is to use four spaces.  However, you can use any number of spaces that you like as long as you are consistent with the spacing.  Avoid using tabs or mixing tabs and spaces - this will lead to consistency issues.  \n",
    "\n",
    "As you write programs, make sure the code blocks are correctly indented -"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "cc215c0d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You can drive!\n",
      "You will need to wait until you are 16.\n"
     ]
    }
   ],
   "source": [
    "age = 17\n",
    "if age >= 16:\n",
    "    print(\"You can drive!\")\n",
    "else:\n",
    "    print(\"You are not old enough to drive.\")\n",
    "print(\"You will need to wait until you are 16.\")  #oops, didn't indent this properly, semantic error"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4af57f38-34cf-419d-b558-47268d232e7a",
   "metadata": {},
   "source": [
    "```python\n",
    "age = 10\n",
    "if age >= 16:\n",
    "    print(\"You can drive!\")\n",
    "else:\n",
    "    print(\"You are not old enough to drive.\")\n",
    "   print(\"You will need to wait until you are 16.\")   #syntax error - indention level not matched\n",
    "```\n",
    "\n",
    "```\n",
    "File <tokenize>:6\n",
    "    print(\"You will need to wait until you are 16.\")   #syntax error - indention level not matched\n",
    "    ^\n",
    "IndentationError: unindent does not match any outer indentation level\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a41a7af",
   "metadata": {},
   "source": [
    "## if elif statement\n",
    "If you have several conditions that you need to evaluate in order, you can use an `if elif` statement. As with the `if` statement, you can have an optional `else` portion at the end of the statement. \n",
    "\n",
    "![](images/ifElifStatement.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "a5abb79b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "What was your score on the test? 88\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "B\n"
     ]
    }
   ],
   "source": [
    "grade = int(input(\"What was your score on the test?\"))\n",
    "if grade >= 90:\n",
    "    print(\"A\")\n",
    "elif grade >= 80:\n",
    "    print(\"B\")\n",
    "elif grade >= 70:\n",
    "    print(\"C\")\n",
    "elif grade >= 60:\n",
    "    print(\"D\")\n",
    "else:\n",
    "    print(\"F\") "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b498901",
   "metadata": {},
   "source": [
    "Python evaluates these if clauses in their listed order.\n",
    "\n",
    "What does the following code produce?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "8fa0492a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "C\n"
     ]
    }
   ],
   "source": [
    "grade = 90\n",
    "if grade >= 70:\n",
    "    print(\"C\")\n",
    "elif grade >= 80:\n",
    "    print(\"B\")\n",
    "elif grade >= 90:\n",
    "    print(\"A\")\n",
    "elif grade >= 60:\n",
    "    print(\"D\")\n",
    "else:\n",
    "    print(\"F\") "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44a9fa43",
   "metadata": {},
   "source": [
    "Not including the colon at the end of an expression will cause the Python interpreter to report a \"SyntaxError\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "934736ed",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "SyntaxError",
     "evalue": "expected ':' (4017835919.py, line 1)",
     "output_type": "error",
     "traceback": [
      "\u001b[0;36m  Cell \u001b[0;32mIn[13], line 1\u001b[0;36m\u001b[0m\n\u001b[0;31m    if True                # missing :    SyntaxError\u001b[0m\n\u001b[0m                           ^\u001b[0m\n\u001b[0;31mSyntaxError\u001b[0m\u001b[0;31m:\u001b[0m expected ':'\n"
     ]
    }
   ],
   "source": [
    "if True                # missing :    SyntaxError\n",
    "    print(\"Message\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35bef069",
   "metadata": {},
   "source": [
    "## Nested Ifs\n",
    "As code blocks can contain any Python statement, `if` statements can be nested.\n",
    "\n",
    "Run the following code block several times, changing the `salary`, `commute_time_min`, and `free_coffee` values to see how the logic works. You should be able to produce each of the four different outputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "f4c9d1ae",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Close location and I like the salary, but I NEED coffee\n"
     ]
    }
   ],
   "source": [
    "salary = 60000\n",
    "commute_time_min = 30\n",
    "free_coffee = False\n",
    "if salary > 50000:\n",
    "    if commute_time_min < 60:\n",
    "        if free_coffee:\n",
    "            print(\"Cool.  I'm going to like this job\")\n",
    "        else:\n",
    "            print(\"Close location and I like the salary, but I NEED coffee\")\n",
    "    else:\n",
    "        print(\"I don't want to drive that far\")\n",
    "else:\n",
    "    print(\"I'm worth more than that\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c726aef",
   "metadata": {},
   "source": [
    "[Step through code on PythonTutor.com](https://pythontutor.com/render.html#code=salary%20%3D%2060000%0Acommute_time_min%20%3D%2030%0Afree_coffee%20%3D%20False%0Aif%20salary%20%3E%2050000%3A%0A%20%20%20%20if%20commute_time_min%20%3C%2060%3A%0A%20%20%20%20%20%20%20%20if%20free_coffee%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20print%28%22Cool.%20%20I'm%20going%20to%20like%20this%20job%22%29%0A%20%20%20%20%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20print%28%22Close%20location%20and%20I%20like%20the%20salary,%20but%20I%20NEED%20coffee%22%29%0A%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20print%28%22I%20don't%20want%20to%20drive%20that%20far%22%29%0Aelse%3A%0A%20%20%20%20print%28%22I'm%20worth%20more%20than%20that%22%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)\n",
    "\n",
    "While producing each of these outputs may have seemed frustrating, this is an essential aspect of testing - following each of the different paths a program can follow.\n",
    "\n",
    "The following table shows the different possibilities.  Notice that we only listed a couple of different values for `salary` and `commute_time_min`.  The program's logic creates equivalence classes for these variables.  Any `salary` value less than or equal to 50,000 is in one class, and any value greater than 50,000 is in another equivalence class.  Similarly, any value for `commute_time_min` < 60 is in one class, and all other values are in another.  `free_coffee` only has two possibilities.\n",
    "\n",
    "| Salary | Commute Time | Free Coffee | Expected Output\n",
    "|  ----: | ----: | :---- | :---- |\n",
    "| 50000  | | | I'm worth more than that\n",
    "| 60000  | 70 |  | I don't want to drive that far\n",
    "| 60000  | 50 | True | Cool.  I'm going to like this job\n",
    "| 60000  | 50 | False | Close location and I like the salary, but I NEED coffee"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9780531a",
   "metadata": {},
   "source": [
    "## Boolean Operators\n",
    "Python offers three Boolean (logical) operators.\n",
    "\n",
    "### And\n",
    "The `and` operator joins two conditional expressions and ensures that both are `True` for the \"true block\" to execute. If either of the conditions is `False`, then the entire `and` expression evaluates to `False`. Note that for \"true\", the actual value returned is the last \"true\" value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "709e7f76",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You will need to take out a loan to buy car insurance!\n"
     ]
    }
   ],
   "source": [
    "age = 21\n",
    "gender = \"Male\"\n",
    "if gender == \"Male\" and age < 25:\n",
    "    print (\"You will need to take out a loan to buy car insurance!\")\n",
    "else:\n",
    "    print (\"Maybe car insurance won't be too expensive for you...\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3cf9ed86",
   "metadata": {},
   "source": [
    "Practice running the above code block with different values for age and gender.\n",
    "\n",
    "### Or\n",
    "The `or` operator also joins two conditional expressions. If at least one of the expressions evaluates to `True`, then it evaluates to `True`. Only if both expressions are `False` will the expression evaluate to `False`.  Note that for \"true\", the actual value returned is the first \"true\" value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "1ee0d6d4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Play golf!\n"
     ]
    }
   ],
   "source": [
    "outlook = \"overcast\"\n",
    "humidity = \"low\"\n",
    "if outlook == \"sunny\" or humidity == \"low\":\n",
    "    print(\"Play golf!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0dbf0ee6",
   "metadata": {},
   "source": [
    "### Not\n",
    "The `not` operator (aka \"logical complement\", \"negation\") reverses the value of a Boolean.  That is, `False` becomes `True`, and `True` becomes `False`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "9ece4d28",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I'm here\n"
     ]
    }
   ],
   "source": [
    "a = 10\n",
    "if not (a > 15):\n",
    "    print (\"I'm here\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34c8a108",
   "metadata": {},
   "source": [
    "## Short-Circuit Evaluation\n",
    "Like most other programming languages, the `and` and `or` operators utilize [short-circuit evaluation](https://en.wikipedia.org/wiki/Short-circuit_evaluation). Once the Python interpreter can determine the outcome of a Boolean expression, the interpreter no longer needs to continue evaluation.\n",
    "\n",
    "For `and`, if the first expression evaluates to `False`, the second expression will not be evaluated as the  result of the operator will always be `False`. \n",
    "\n",
    "For `or`, if the first expression evaluates to `True`, the second expression will not be evaluated as the result of the operator will always be `True`.\n",
    "\n",
    "Note: `def` allows us to create functions that the next notebook will present."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "119a9517",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "evaluating 1 False\n",
      "expr #1 was false\n",
      "\n",
      "evaluating 1 True\n",
      "expr #2 was True\n",
      "\n",
      "evaluating 1 False\n",
      "evaluating 2 True\n",
      "expr #3 was True\n",
      "\n"
     ]
    }
   ],
   "source": [
    "def expr(num, result):\n",
    "    print ('evaluating', num, result)\n",
    "    return result\n",
    "\n",
    "if expr(1,False) and expr(2,True):\n",
    "    print (\"expr #1 was True\\n\")\n",
    "else:\n",
    "    print (\"expr #1 was false\\n\")\n",
    "    \n",
    "if expr(1,True) or expr(2,True):\n",
    "    print (\"expr #2 was True\\n\")\n",
    "else:\n",
    "    print (\"expr #2 was false\\n\")\n",
    "    \n",
    "if expr(1,False) or expr(2,True):\n",
    "    print (\"expr #3 was True\\n\")\n",
    "else:\n",
    "    print (\"If #3 was false\\n\")   "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7e4a823",
   "metadata": {},
   "source": [
    "The result of a logical expression is not always `True` or `False`;  the Python interpreter returns the last value evaluated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "c7b5e26e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\n"
     ]
    }
   ],
   "source": [
    "a = 0\n",
    "b = 3\n",
    "x = b or a\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bfcf7fd0",
   "metadata": {},
   "source": [
    "If you need a boolean result, then force the conversion to boolean with the built-in function `bool()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "7fa22ce1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n"
     ]
    }
   ],
   "source": [
    "print(bool(b or a))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34ceec4c",
   "metadata": {},
   "source": [
    "Here is an example where the behavior would be useful:\n",
    "```\n",
    "key = load_secret_key() or 'randomStringValue'\n",
    "```\n",
    "The Python interpreter will try to load the secret key from some configuration using the function `load_secret_key()`. If that function does not return a valid value, then the interpreter assigns the value of `'randomStringValue'` to `key`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddc3f9a8",
   "metadata": {},
   "source": [
    "Programmers can use multiple `and` and `or` clauses in the same expression.  Use parenthesis to ensure the correct order of operations / logical meaning."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "981f74ff",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "evaluating 1 True\n",
      "evaluating 2 False\n",
      "evaluating 3 True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "def expr(num, result):\n",
    "    print ('evaluating', num, result)\n",
    "    return result\n",
    "\n",
    "print (expr(1,True) and (expr(2,False) or expr(3,True)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e578330c",
   "metadata": {},
   "source": [
    "## Footnote\n",
    "Historically, in Python, a group of statements executed together is called a \"suite\".  However, most other programming languages and even the [Python Grammer Specification](https://docs.python.org/3/reference/grammar.html) refer to this as a block. Therefore, we will use the term \"block\" for this class."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c689369",
   "metadata": {},
   "source": [
    "## Variables Revisited\n",
    "Think of variables as values with names; those names describe what those values represent.\n",
    "\n",
    "Variables appear within our Python programs under one of these three usages:\n",
    "1. To declare and set a value to the variable. \n",
    "2. To assign the variable to a new value (object)\n",
    "3. Use the variable - whether in an expression or outputting the value in some fashion\n",
    "\n",
    "As we declare a variable, remember that the name must be a legal identifier: The variable name must start with a letter or an underscore.  The variable name only contains letters, numbers, and underscores.  The variable name can not be one of Python's reserved words.\n",
    "\n",
    "Before we use a variable, we must declare it first and assign some value to it.\n",
    "\n",
    "To assign a value to a variable, we use an assignment operator:\n",
    "<pre>\n",
    "a = 45 * 10\n",
    "</pre>\n",
    "Programmers read that line of code as \"set a to the value of 45 times 10\" or \"a is assigned <i>expression</i>\".\n",
    "\n",
    "The first time a variable is assigned a value is also known as initializing the variable.\n",
    "\n",
    "Once a variable has been assigned a value or to an existing object, programmers can then use that variable in other statements and expressions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "ffbaec4f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "450\n",
      "15\n"
     ]
    }
   ],
   "source": [
    "a = 45 * 10;\n",
    "print(a)\n",
    "b = a // 30;\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6cbb54f0",
   "metadata": {},
   "source": [
    "Unlike many other programming languages, Python is dynamically-typed. Therefore, we can change the variable's type by assigning different values(objects) to the variable."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6bf8ca5",
   "metadata": {},
   "source": [
    "## Float Equality\n",
    "While it is possible to test float variables and literals for equality with `==`, you need to be careful of the particular situation.  As mentioned in a previous notebook, floating-point numbers do not have an exact representation.  If the program performs some calculations, the results may not be identical.  For instance -"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "2565c10e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.1 + 2.2 == 3.3: False\n"
     ]
    }
   ],
   "source": [
    "print(\"1.1 + 2.2 == 3.3:\",1.1 + 2.2 == 3.3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e627a582",
   "metadata": {},
   "source": [
    "To work around this, you can check that the two numbers are within a given tolerance of each other. While it may appear that `abs(f1 - f2) <= tolerance` suffices, this approach can still be problematic given the magnitude of the numbers.  Python now provides `isclose(a,b)` in the math module - [math.isclose()](https://docs.python.org/3/library/math.html#math.isclose).  You can specify different tolerances as an optional parameter to that function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "2431b8be",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.1 + 2.2 == 3.3: True\n"
     ]
    }
   ],
   "source": [
    "import math\n",
    "print(\"1.1 + 2.2 == 3.3:\",math.isclose(1.1 + 2.2,3.3))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f68bda1",
   "metadata": {},
   "source": [
    "## Suggested LLM Prompts\n",
    "- Explain the purpose and syntax of if statements in Python, providing examples of different \n",
    "  scenarios where they can be used, such as decision-making, flow control, and conditional \n",
    "  execution of code blocks. Use a financial example.\n",
    "- Discuss the concept of relational operators in Python (e.g., ==, !=, >, <, >=, <=) and their \n",
    "  role in evaluating boolean expressions within if statements. Include examples demonstrating \n",
    "  their usage and how they can be combined using logical operators (and, or, not).\n",
    "- Create a financial scenario where nested if statements are necessary, and walk through the process\n",
    "  of writing and understanding nested if-elif-else structures. Discuss best practices for managing \n",
    "  complexity and readability when dealing with deeply nested conditions.\n",
    "- Present a real-world coding challenge or problem in finance that requires the use of if statements, \n",
    "  relational operators, and potentially elif and else clauses. Guide the programmer through the \n",
    "  process of analyzing the problem, identifying the necessary conditions, and implementing an \n",
    "  appropriate solution using the concepts covered in the previous prompts.\n",
    "- Explore the concept of short-circuiting in Python's logical operators (and, or) and how this \n",
    "  behavior can be leveraged to write more efficient and concise conditional expressions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "880a09c3",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "\n",
    "1. What is the type and value of x?\n",
    "\n",
    "   ```python\n",
    "   a = 0\n",
    "   b = 10\n",
    "   x = b or a\n",
    "   ```\n",
    "2. Why is the order of `if` and `elif` clauses important?\n",
    "3. Explain short-circuit evaluation for `and` and `or` clauses.\n",
    "4. How are code blocks identified in Python?\n",
    "5. What is the difference between `if/elif` statement(s) and a series of `if` statements?\n",
    "6. Why is testing for equality with float numbers problematic?  How do we work around such situations?\n",
    "\n",
    "[answers](answers/rq-06-answers.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2667f27f",
   "metadata": {},
   "source": [
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21525bf3",
   "metadata": {},
   "source": [
    "1. Paycheck<br>\n",
    "   Write a program that computes a simple weekly paycheck for an individual. Prompt the user to enter the number of hours worked and the pay rate. If an individual works over 40 hours, then the individual is paid at 1.5 times the hourly rate for the hours worked over 40. Print the initial total. Of course, the government taxes the individualʼs pay. Assume the tax rate is 20%. Print the amount withheld for taxes. Then print the net amount to be paid to the individual."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c53ac97a",
   "metadata": {},
   "source": [
    "2. Stock Gain (or loss)<br>\n",
    "   Write a program that allows the user to enter two successive closing prices for a stock.  Compute the daily percentage gain or loss. If the gain was positive, print the message \"The stock price increased by\" and the percentage increase.\n",
    "If the gain was negative, print the message \"The stock price decreased by\" and the percentage decrease.\n",
    "Otherwise, print \"No change to the stock price.\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24d3f831",
   "metadata": {},
   "source": [
    "3. Body Mass Index(BMI) Caclulator<br>\n",
    "   [BMI](https://en.wikipedia.org/wiki/Body_mass_index) is a crude measurement calculated from \n",
    "   the weight and height of a person. The medical field uses the value as a potential risk factor \n",
    "   for several health issues as its quickly obtainable. For this exercise, you will prompt the \n",
    "   user for the necessary values, calculate the BMI, and display the BMI with its associated category. \n",
    "\n",
    "   $ BMI = \\frac{mass}{height^2}  $\n",
    "   where the mass is specified in kilograms and the height in meters.\n",
    "   | Category      | BMI         |\n",
    "   |---------------|-------------|\n",
    "   | underweight   | < 18.5      |\n",
    "   | normal weight | 18.5 - 24.9 |\n",
    "   | overweight    | 25.0 - 29.9 |\n",
    "   | obese         | >= 30.0     |\n",
    "\n",
    "   First, prompt the user the if they want to use imperial or metric units:\n",
    "   ```\n",
    "   Enter values in (i)mperial or (m)etric units: \n",
    "   ```\n",
    "   The user will need to enter 'i' or 'm'. If the user does not enter an appropriate value, use the built-in function, `exit()` to immediately stop the program.\n",
    "   Use `1` as an argument value to the function. (While it will be discussed later, return values from programs/processes are used to tell the calling program or\n",
    "   environment whether or not an error has occured. 0 signifies success, a non-zero integer values indicate some sort of an error.)\n",
    "   \n",
    "   Next if they select imperial, use these prompts:\n",
    "   ```text\n",
    "   Enter the individual's weight in pounds:\n",
    "   Enter the individual's height in inches:\n",
    "   ```\n",
    "   For metric, use these prompts\n",
    "   ```text\n",
    "   Enter the individual's weight in kilograms:\n",
    "   Enter the individual's height in meters:\n",
    "   ```\n",
    "   Calculate the BMI. To convert from poounds to kilograms, multiply by 0.453592. To convert from inches to meters, multiply by 0.0254.\n",
    "   Display the BMI with the associated category in parenthesis. Do not trim or round the values.\n",
    "   ```text\n",
    "   22.91 (normal weight)\n",
    "   ```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5836a6af",
   "metadata": {},
   "source": [
    "4. Failed short-circuit evaluation?<br>\n",
    "   In the following code block, why does it appear that the rules for the short-circuit evaluation are not followed? And the result is `True`?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "02f9262e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "evaluating 1 False\n",
      "evaluating 2 True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "def test(num, result):\n",
    "    print ('evaluating', num, result)\n",
    "    return result\n",
    "\n",
    "print (test(1,\"False\") and test(2,\"True\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cfb4f32a-00e3-4c8b-83aa-6957bb413b41",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
